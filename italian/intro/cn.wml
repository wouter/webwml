#use wml::debian::template title="Sito web Debian in altre lingue" MAINPAG=true
#use wml::debian::toc
#use wml::debian::translation-check translation="de75d9bf72c9046fb3bbae3fcf7df540c6e2b420" maintainer="Giuseppe Sacco"

<define-tag toc-title-formatting endtag="required">%body</define-tag>
<define-tag toc-item-formatting endtag="required">[%body]</define-tag>
<define-tag toc-display-begin><p></define-tag>
<define-tag toc-display-end></p></define-tag>

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#intro">Content Navigation</a></li>
    <li><a href="#howtoset">Come impostare la lingua del browser</a></li>
    <li><a href="#override">Come scavalcare le impostazioni</a></li>
    <li><a href="#fix">Risolvere problemi</a></li>
  </ul>
</div>

<h2><a id="intro">Content Navigation</a></h2>

<p>
Un gruppo di <a href="../devel/website/translating">traduttori</a>
lavora sul sito Debian per ampliarlo con un numero crescente di
lingue. Ma come funziona il cambio di lingua del browser web?
Uno standard chiamato
<a href="$(HOME)/devel/website/content_negotiation">negoziazione dei contenuti</a>
permette agli utenti di impostare la/e lingua/a preferita/e per i
contenuti web. La versione che loro vedono è negoziata tra il browser
web e il server web: il browser invia le sue preferenze al server, il quale
decide che versione restituire (in base alle preferenze e alle
versioni disponibili).
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="http://www.w3.org/International/questions/qa-lang-priorities">Maggiori informazio sul sito W3C</a></button></p>

<p>
Non tutti conoscono la negoziazione dei contenuti, per questo i collegamenti
in fondo ad ogni pagina di Debian puntano alle altre versioni della stessa pagina.
Notare che selezionando una lingua diversa da questa lista la si imposta solo
per la pagina attuale. Non cambia la lingua predefinita del browser web. Se poi si
segue un collegamento verso una nuova pagina web, quella verrà caricata nella lingua
predefinita.
</p>

<p>
Per cambiare la lingua predefinita ci sono due possibilità:
</p>

<ul>
  <li><a href="#howtoset">Configurare il browser web</a></li>
  <li><a href="#override">Scavalcare le impostazioni predefinita per la lingua del browser</a></li>
</ul>

<p>
Andare direttamente alle istruzioni di configurazione per questi browser web: <toc-display />
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>La lingua originale del sito web
Debian è l'inglese, quindi è una buona idea di aggiungere inglese (<code>en</code>)
alla fine dell'elenco delle lingue. Questo funge un po' da estrema ratio nel caso
che una pagina non sia tradotta in nessuna delle lingue che si preferiscono.</p>
</aside>

<h2><a id="howtoset">Configurare il browser web</a></h2>

<p>
Prima di mostrare come configurare l'impostazione della lingua con vari
browser, ecco alcune note generali. Primo, è una buona idea aggiungere tutte le lingue
che si conoscono nella lista delle lingue predefinite. Per esempio, ne si è
madrelingua italiano si può scegliere <code>it</code> come prima lingua,
seguito da inglese con il codice lingua <code>en</code>.
</p>

<p>
Secondo, in alcuni browser si può inserire il codice della lingua anziché
selezionarlo da un elenco. Se questo è il proprio caso, tenere presente che
inserire una lista come <code>it, en</code> non definirà la preferenza. Invece
darà una elenco di lingue con la stessa priorità e il server web di ignorare
l'ordine e scegliere sempre una qualsiasi delle due. Se si vuole indicare
una vera preferenza si deve lavorare con il cosiddetto valore della qualità,
cioè un numero reale tra 0 e 1. A numero più alti corrisponde una priorità
maggiore. Se torniamo all'esempio precedente con le lingue italiano e inglese,
si può modificare l'esempio soprastante in questo modo:
</p>

<pre>
it; q=1.0, en; q=0.5
</pre>

<h3>Fare attenzione con i codici delle nazioni</h3>

<p>
Un server web che riceva una richiesta di un documento con ligua
preferita <code>en-GB, it</code> non restituisce sempre la versione
inglese prima della italiana. Lo farà solo se una pagina con estensione
<code>en-gb</code> esista. Ma in realtà funziona al contrario: un server
può restituire una pagina <code>en-gb</code> se nell'elenco delle lingue
preferite è indicato <code>en</code>.
</p>

<p>
Per questo è sconsigliato inserire il codice di due lettere della nazione
come in <code>en-GB</code> o <code>en-US</code>, a meno che non ci sia una
buona ragione per farlo. Se lo si aggiunge, accertarsi di aggiungere anche
quello senza la nazione: <code>en-GB, en, it</code>
</p>

<p style="text-align:center">
  <button type="button">
  <span class="fas fa-book-open fa-2x"></span>
    <a href="https://httpd.apache.org/docs/current/content-negotiation.html">Maggiori informazioni sulla negoziazione dei contenuti</a>
  </button>
</p>

<h3>Istruzioni per alcuni browser web</h3>

<p>
Abbiamo compilato un elenco di browser web popolari con le istruzioni
su come cambiare la lingua predefinita per il contenuto web all'interno
delle impostazioni:
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="chromium">Chrome/Chromium</toc-add-entry></strong><br>
  In alto a destra, aprire il menu e fare click su <em>Impostazioni</em> -&gt; <em>Avanzate</em>
-&gt; <em>Lingue</em>. Aprire il menu <em>Lingue</em> per vedere l'elenco delle lingue.
Fare click sui tre puntini vicino ad un elemento per cambiare la posizione. Si possono
anche aggiungere lingue, se necessario.</li>
        <li><strong><toc-add-entry name="elinks">ELinks</toc-add-entry></strong><br>
  Impostare la lingua predefinita (dell'interfaccia utente) tramite <em>Impostazioni</em> -&gt;
  <em>Lingua</em> cambia anche la lingua richiesta ai siti web. Si può cambiare questo
  comportamento e regolare meglio l'<em>intestazione Accept-Language</em> tramite
  <em>Impostazioni</em> -&gt; <em>Gestore opzioni</em> -&gt; <em>Protocolli</em> -&gt;
  <em>HTTP</em></li>
        <li><strong><toc-add-entry name="epiphany">Epiphany</toc-add-entry></strong><br>
  Aprire <em>Preferenze</em> dal menu principale e passare alla maschera <em>Lingue</em>.
Qui si possono aggiungere, togliere e riordinare le lingue.</li>
        <li><strong><toc-add-entry name="mozillafirefox">Firefox</toc-add-entry></strong><br>
  Nella barra dei menu in alto, aprire <em>Preferenze</em>. Scorrere in basso fino a
<em>Lingua e aspetto</em> -&gt; <em>Lingua</em> nel pannello <em>Generale</em>. Fare click
sul bottone <em>Scelta</em> per impostare la lingua predefinita per visualizzare i siti
web. Nello stesso dialogo è anche possibile aggiungere, togliere e riordinare le lingue.</li>
       <li><strong><toc-add-entry name="ibrowse">IBrowse</toc-add-entry></strong><br>
  Andare in <em>Preferenze</em> -&gt; <em>Impostazioni</em> -&gt; <em>Rete</em>.
<em>Lingua accettata</em> probabilmente mostra un * che è la lingua predefinita.
Se si fa click sul bottone <em>Locale</em>, si dovrebbe essere in grado di aggiungere
la propria lingua preferita. Altrimenti la si può aggiungere manualmente.</li>
        <li><strong><toc-add-entry name="icab">iCab</toc-add-entry></strong><br>
  <em>Modifica</em> -&gt; <em>Preferenze</em> -&gt; <em>Browser</em> -&gt;
<em>Caratteri, lingue</em></li>
        <li><strong><toc-add-entry name="iceweasel">IceCat (Iceweasel)</toc-add-entry></strong><br>
  <em>Modifica</em> -&gt; <em>Preferenze</em> -&gt; <em>Contenuto</em> -&gt; <em>Lingue</em>
-&gt; <em>Scelta</em></li>
        <li><strong><toc-add-entry name="ie">Internet Explorer</toc-add-entry></strong><br>
  Fare click sull'icona <em>Strumenti</em>, selezionare <em>Opzioni Internet</em>, passare
al pannello <em>Generale</em> e fare click sul pulsante <em>Lingue</em>. Fare click su
<em>Impostare le preferenze sulla lingua</em> e, nel dialogo che appare, si potrà
inserire, cancellare e riordinare l'elenco delle lingue.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="konqueror">Konqueror</toc-add-entry></strong><br>
        Modificare il file <em>~/.kde/share/config/kio_httprc</em> e includere la linea seguente:<br>
        <code>Languages=it;q=1.0, en;q=0.5</code></li>
        <li><strong><toc-add-entry name="lynx">Lynx</toc-add-entry></strong><br>
        Modificare il file <em>~/.lynxrc</em> e includere la linea seguente:<br>
        <code>preferred_language=it; q=1.0, en; q=0.5</code><br>
        In alternativa di possono aprire le impostazioni del browser premento [O]. Scendere giù
        fino a <em>Lingua preferita</em> e aggiungere quanto sopra.</li>
        <li><strong><toc-add-entry name="edge">Microsoft Edge</toc-add-entry></strong><br>
        <em>Impostazioni e altro</em> -&gt; <em>Impostazioni</em> -&gt; <em>Lingua</em> -&gt; <em>Aggiungi lingue</em><br>
        Fare click sul bottone con tre puntini vicino ad una lingua per altre opzioni e per cambiare l'ordinamento.</li>
        <li><strong><toc-add-entry name="opera">Opera</toc-add-entry></strong><br>
        <em>Impostazioni</em> -&gt; <em>Browser</em> -&gt; <em>Lingua</em> -&gt; <em>Lingue preferite</em></li>
        <li><strong><toc-add-entry name="safari">Safari</toc-add-entry></strong><br>
        Safari usa le impostazioni del sistema su macOS e iOS, quindi per impostare la lingua preferita
        aprire <em>Preferenze di sistema</em> (macOS) o <em>Impostazioni</em> (iOS).</li>
        <li><strong><toc-add-entry name="w3m">W3M</toc-add-entry></strong><br>
        Premere [O] per aprire il <em>pannello delle opzioni</em>, scendere giù fino a <em>Impostazioni
        di rete</em> -&gt; <em>intestazione Accept-Language</em>. Premere [Invio] per cambiare le
        impostazioni (per esempre <code>it; q=1.0, en; q=0.5</code>) e confermare con [Invio].
        Scendere fino in basso su [OK] per salvare le impostazioni.</li>
        <li><strong><toc-add-entry name="vivaldi">Vivaldi</toc-add-entry></strong><br>
        Andare su <em>Impostazioni</em> -&gt; <em>Generale</em> -&gt; <em>Lingua</em> -&gt; <em>Lingue accettate</em>,
        fare click su <em>Aggiungi lingua</em> e selezionarne una dal menu. Usare i tasti freccia
        per cambiare l'orinamento.</li>
      </ul>
    </div>
  </div>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Nonostante sia sempre meglio impostare la lingua preferita
nella configurazione del browser, è anche possibile superare quella impostazione con un cookie.</p>
</aside>

<h2><a id="override">Come superare le impostazioni</a></h2>

<p>
Se, per qualsiasi motivo, non fosse possibile definire la lingua preferita
nelle impostazioni del browser o del dispositivo, si può procedere, come
ultima spiaggia, all'utilizzo di un cookie. Fare click su uno dei bottoni
qui sotto per mettere una singola lingua in cima all'elenco.
</p>

<p>
Notare che questo imposterà un <a
href="https://en.wikipedia.org/wiki/HTTP_cookie">cookie</a>. Il proprio browser
lo cancellerà automaticamente se non si visita il sito per un mese. Naturalmente
lo si può sempre cancellare manualmente nel proprio browser o premento il
pulsante <em>Browser default</em>.
</p>

<protect pass=2>
<: print language_selector_buttons(); :>
</protect>

<h2><a id="fix">Risolvere problemi</a></h2>

<p>
Può succedere che il sito Debian appaia nella lingua sbagliata nonostante
tutti gli sforzi per impostare la lingua preferita. Il nostro suggerimento
è di pulire la <em>cache</em> locale (sia su disco che in memoria) del browser prima
di provare a ricaricare la pagina. Se si è assolutamente certi di aver
<a href="#howtoset">configurato il browser</a> correttamente, allora una <em>cache</em>
mal configurata o in errore può essere la causa del problema. Questo sta
divenendo un problema molto serio, man mano che gli ISP vedono la <em>cache</em> come
un metodo per ridurre il loro traffico di rete. Leggere la <a href="#cache">sezione</a>
sui server proxy anche se non se ne sta usando nessuno.
</p>

<p>
Ovviamente è sempre possibile che ci sia un problema con <a
href="https://www.debian.org/">www.debian.org</a>. Anche se solo una minima
parte delle segnalazioni di problemi, a proposito della lingua, arrivate negli
ultimi anni si è rivelata un problema nostro, è comunque possibile. Per questo
suggeriamo che dopo aver controllato le proprie impostazioni e la configurazione
della <em>cache</em>, ci si <a href="../contact">contatti</a>. Se <a
href="https://www.debian.org/">https://www.debian.org/</a> funziona, ma uno dei
<a href="https://www.debian.org/mirror/list">mirror</a> no, allora comunicare
anche questo in modo da risolvere la cosa con chi gestisce il mirror.
</p>

<h3><a name="cache">Problemi potenziali con i server proxy</a></h3>

<p>
I server proxy sono sostanzialmente dei server web che non hanno dei
contenuti proprii. Si trovano a metà tra utenti e server web, ricevono
le richieste di pagine web e scaricano le pagine. Poi inoltrano la
pagina al browser web dell'utente, ma ne tengono anche una copia locale
che viene usata per richieste successive. Questo può effettivamente
tradursi in una forte riduzione del traffico di rete quando molti utenti
chiedono la stessa pagina.
</p>

<p>
Nonostante questa possa essere una buona idea nella maggior parte dei
casi, è anche la causa di problemi quando la <em>cache</em> non funziona. In
particolare, alcuni vecchi proxy non capiscono la negoziazione dei
contenuti. Questo fa sì che mettano in <em>cache</em> la pagina in una sola lingua
e la restituiscano anche quando viene successivamente richiesta una
lingua diversa. L'unica soluzione è aggiornare o sostituire il
software di <em>cache</em>.
</p>

<p>
Storicamente i server proxy erano usati solamente quando le persone
configuravano il loro browser per usarli. Purtroppo non è più così.
Gli ISP posso dirigere tutte le richieste HTTP attraverso un proxy
trasparente. Se il proxy non gestisce correttamente la negoziazione
dei contenuti, gli utenti possono ricevere la pagina nella lingua
sbagliata. L'unico modo per risolvere questo problema è di
protestare con il proprio ISP al fine di fare aggiornare o
sostituire il loro software.
</p>
