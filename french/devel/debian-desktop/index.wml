#use wml::debian::template title="Debian comme poste de travail"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="385b72248b2434177dbd472f7c322a759a6a8dd0" maintainer="Jean-Pierre Giraud"
# Original translation by Yann Forget

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#philosophy">Notre philosophie</a></li>
<li><a href="#help">Comment pouvez-vous aider ?</a></li>
<li><a href="#join">Rejoignez-nous !</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian Desktop est un groupe de volontaires qui veulent créer le meilleur système d'exploitation possible pour un poste de travail pour une utilisation personnelle ou professionnelle. Notre devise est <q>Des logiciels qui fonctionnent, tout simplement</q>. En bref, notre objectif est d'apporter Debian, GNU et Linux à tout le monde.</p>
</aside>

<h2><a id="philosophy">Notre philosophie</a></h2>

<p>
Nous reconnaissons que de nombreux
<a href="https://wiki.debian.org/DesktopEnvironment">environnements de bureau</a>
existent et nous prendrons en charge leur utilisation – et nous veillerons à ce
qu’ils fonctionnent bien dans Debian. Notre objectif est de rendre les
interfaces graphiques faciles à utiliser pour les débutants, tout en permettant
aux experts de modifier les choses comme ils le souhaitent.
</p>

<p>
Nous essaierons de faire en sorte que les logiciels soient configurés pour
l'utilisation la plus courante d'un poste de travail. Par exemple, le compte
d’utilisateur ordinaire ajouté lors de l’installation doit permettre de regarder
des vidéos, d'écouter de la musique, d'imprimer et de gérer le système avec
sudo. Aussi, nous aimerions que les questions posées par
<a href="https://wiki.debian.org/debconf">debconf</a> (le système de gestion de
la configuration de Debian) soient réduites au strict minimum. Durant
l'installation, il est inutile de présenter des détails techniques complexes.
Nous essaierons plutôt de nous assurer que les questions de debconf aient du sens
pour les utilisateurs. Le débutant peut même ne pas en deviner le sens. L'expert,
néanmoins, sera vraiment heureux de pouvoir configurer son environnement de
bureau après que l’installation soit terminée.
</p>

<h2><a id="help">Comment pouvez-vous aider ?</a></h2>

<p>
Nous recherchons des <em>personnes motivées</em> qui font avancer les choses
Vous n'avez pas besoin d'être un développeur Debian (DD) pour proposer des
rustines ou commencer à faire des paquets. Le cœur de l'équipe Debian Desktop
s'assure que votre travail sera intégré. Ainsi, voici plusieurs choses que vous
pouvez faire : 
</p>

<ul>
  <li>Tester l'environnement de bureau par défaut (ou n'importe quel autre environnement) de la prochaine publication. Il suffit d'installer une des <a href="$(DEVEL)/debian-installer/">images de test</a> et envoyer vos commentaires à la <a href="https://lists.debian.org/debian-desktop/">liste de diffusion debian-desktop</a> ;</li>
  <li>Rejoindre <a href="https://wiki.debian.org/DebianInstaller/Team">l'équipe de l'installateur Debian</a> et aider à améliorer <a href="$(DEVEL)/debian-installer/">l'installateur Debian</a> – l'interface GTK+ a besoin de main d’œuvre ;</li>
  <li>Vous pouvez aider <a href="https://wiki.debian.org/Teams/DebianGnome">l'équipe Debian GNOME</a>, les <a href="https://qt-kde-team.pages.debian.net/">mainteneurs de Debian Qt/KDE et l'équipe Debian KDE Extras</a> ou le <a href="https://salsa.debian.org/xfce-team/"> groupe Debian Xfce</a> pour aider à l'empaquetage, à la correction de bogues, à la documentation, aux tests, etc. ;</li>
  <li>Aider à l'amélioration de <a href="https://packages.debian.org/debconf">debconf</a> en réduisant la priorité des questions, en retirant des paquets les écrans non nécessaires ou encore en rendant ceux qui sont nécessaires faciles à comprendre ;</li>
  <li>Et si vous avez des compétences en conception graphique, pourquoi ne pas travailler à l'<a href="https://wiki.debian.org/DebianDesktop/Artwork">initiative artistique pour le bureau Debian</a>.</li>
</ul>

<h2><a id="join">Rejoignez-nous !</a></h2>

<aside class="light">
  <span class="fa fa-users fa-4x"></span>
</aside>

<ul>
  <li><strong>Wiki :</strong> visitez la page <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a> du Wiki (certains articles peuvent être dépassés).</li>
  <li><strong>Liste de diffusion :</strong> discutez avec nous sur la liste de diffusion <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.</li>
  <li><strong>Canal IRC :</strong> discutez en temps réel avec nous sur IRC. Rejoignez le canal #debian-desktop sur <a href="http://oftc.net/">OFTC IRC</a> (irc.debian.org).</li>
</ul>

