#use wml::debian::translation-check translation="b814e5f8f89e2086e558034fbf26a53d5f985512" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Handshake donne 300 000 dollars à Debian</define-tag>
<define-tag release_date>2019-03-29</define-tag>
#use wml::debian::news
# $Id$

<p>
En 2018, le projet Debian a reçu une donation de 300 000 dollars de
<a href="https://handshake.org/">Handshake</a>, une organisation qui développe
un système racine expérimental de noms de domaine pair-à-pair.
</p>

<p>
Cette contribution financière importante aidera Debian à continuer le plan
de remplacement de matériel conçu par les
<a href="https://wiki.debian.org/Teams/DSA">administrateurs système Debian (DSA)</a>,
renouvelant les serveurs et autres composants matériels, et donc améliorant
la fiabilité de l'infrastructure de développement et celle de la communauté.
</p>

<p>
<q><em>Sincère grand merci à la fondation Handshake pour son soutien au projet
Debian</em></q> a déclaré Chris Lamb, le chef du projet Debian.<q><em> Des
contributions telles que la leur permettent à un grand nombre de contributeurs
divers de partout dans le monde de travailler ensemble à atteindre notre
objectif commun de développer un système d'exploitation <q>universel</q>
vraiment libre.</em></q>
</p>

<p>
Handshake est un protocole de nommage décentralisé et sans permission,
compatible avec DNS, dans lequel chaque pair valide et est chargé de gérer
la zone racine dans le but de créer une alternative aux autorités de
certification existantes.
</p>

<p>
Le projet Handshake, ses sponsors et contributeurs reconnaissent que le
logiciel libre et à source ouvert constitue une partie essentielle des
fondations d'Internet et de leur projet, aussi ils ont décidé de réinvestir
dans le logiciel libre en donnant 10 200 000 dollars à divers développeurs et
projets de logiciels libres (FLOSS), ainsi qu'à des organisations sans but
lucratif et des universités qui soutiennent le développement du logiciel libre.
</p>

<p>
Merci beaucoup, Handshake, pour votre soutien !
</p>

<h2>À propos de Debian</h2>

<p>
Le projet Debian est une association de développeurs de logiciels
libres qui offrent volontairement leur temps et leurs efforts pour
produire le système d'exploitation complètement libre Debian.
</p>


<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de Debian <a
href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier électronique
à &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>
