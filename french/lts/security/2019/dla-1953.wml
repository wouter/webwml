#use wml::debian::translation-check translation="230513ec2eb088eb30470ebcc38b8f9c86908757" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Clamav, le moteur antivirus au source libre, est sujet aux vulnérabilités de
sécurité suivantes.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12625">CVE-2019-12625</a>

<p>Vulnérabilité de déni de service (DoS), conséquence de temps d’analyse trop
longs dûs à des bombes zip non récursives. Entre autres, ce problème était
atténué par l’introduction d’une limite de temps d’analyse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12900">CVE-2019-12900</a>

<p>Écriture hors limites dans la bibliothèque bzip2 NSIS de ClamAV lors de
l’essai de décompression dans le cas ou le nombre de sélecteurs excédait la
limite maximale définie par la bibliothèque.</p>

<p>Cette mise à jour déclenche une transition de libclamav7 à libclamav9. En
conséquence, plusieurs autres paquets seront recompilés à partir du paquet
corrigé après la publication de cette mise à jour : dansguardian, havp,
python-pyclamav et c-icap-modules.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.101.4+dfsg-0+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets clamav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1953.data"
# $Id: $
