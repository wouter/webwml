#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans le moteur de
servlet et JSP de Tomcat.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5647">CVE-2017-5647</a>

<p>Un bogue dans le traitement de requêtes en pipeline lorsque <q>send
file</q> était utilisé, aboutissait à ce que ces requêtes soient perdues lorsque le
traitement <q>send file</q> de la précédente requête finissait. Cela peut
aboutir à des réponses apparaissant être envoyées à la mauvaise requête.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5648">CVE-2017-5648</a>

<p>Il a été remarqué que certains appels d’écouteurs d’application
(<q>listeners</q>) n’utilisaient pas l’objet façade approprié. Lors de
l’exécution d’une application non fiable sous SecurityManager, il était alors
possible pour cette application non fiable de conserver une référence à l’objet
de la requête ou de la réponse et donc d'accéder ou de modifier les informations
associées avec une autre application web.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 7.0.28-4+deb7u12.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-924.data"
# $Id: $
