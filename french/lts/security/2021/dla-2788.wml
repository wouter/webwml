#use wml::debian::translation-check translation="19b97829e0113715437624611084d50a7f580ce4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Des chercheurs de la NSA (America National Security Agency) ont relevé une
vulnérabilité de déni de service dans strongSwan, une suite IKE/IPsec.</p>

<p>Une fois que le cache de certificats en mémoire est plein, il essaie de
remplacer de manière aléatoire les entrées les moins utilisées. Selon la
valeur aléatoire générée, cela pourrait conduire à un dépassement d'entier
qui aboutirait à un double déréférencement et à un appel utilisant une
mémoire hors limites, conduisant très vraisemblablement à une erreur de
segmentation.</p>

<p>L’exécution de code à distance ne peut pas être écartée complètement, mais
les attaquants ne contrôlent pas la mémoire déréférencée, aussi cela semble
improbable à ce stade.</p>


<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 5.5.1-4+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets strongswan.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de strongswan,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/strongswan">\
https://security-tracker.debian.org/tracker/strongswan</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2788.data"
# $Id: $
