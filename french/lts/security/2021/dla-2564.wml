#use wml::debian::translation-check translation="29764ac3c02e2e15048c7eb4eae96aa68c64a0bb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Alex Birnberg a découvert une vulnérabilité de script intersite (XSS) dans
le cadriciel d’applications Horde, plus précisément dans son API Text Filter.
Un attaquant pourrait contrôler la boîte aux lettres de l’utilisateur en
envoyant un courriel contrefait.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26929">CVE-2021-26929</a>

<p>Un problème d’XSS a été découvert dans <q>Horde Groupware Webmail Edition</q>
(dans laquelle la bibliothèque Horde_Text_Filter est utilisée). Un attaquant
peut envoyer un message de courriel en texte simple, avec du JavaScript encodé
sous forme de lien, ou un courriel mal géré par le préprocesseur dans Text2html.php
parce que l’utilisation personnalisée de \x00\x00\x00 et \x01\x01\x01 interfère
avec les défenses XSS.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 2.3.5-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php-horde-text-filter.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php-horde-text-filter, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php-horde-text-filter">\
https://security-tracker.debian.org/tracker/php-horde-text-filter</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2564.data"
# $Id: $
