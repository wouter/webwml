#use wml::debian::translation-check translation="d6b66ab8ea80395a61d07daaa5c4359f4e78c3ee" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités de sécurité ont été corrigés dans la servlet Tomcat et le
moteur JSP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12418">CVE-2019-12418</a>

<p>Lorsqu’Apache Tomcat est configuré avec le JMX Remote Lifecycle Listener,
un attaquant local sans accès au processus ou aux fichiers de configuration de
Tomcat, est capable de manipuler des registres RMI pour réaliser une attaque
d’homme du milieu pour capter les noms et mots de passe d’utilisateur utilisés
pour accéder à l’interface JMX. L’attaquant peut alors utiliser ces
accréditations pour accéder à l’interface JMX et obtenir un contrôle complet de
l’instance de Tomcat.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17563">CVE-2019-17563</a>

<p>Lors de l’utilisation de FORM avec authentification Apache Tomcat, il
existait une fenêtre étroite où un attaquant pourrait réaliser une attaque de
fixation de session. La fenêtre était considérée comme trop étroite pour être
exploitable mais, péchant par principe de précaution, ce problème a été traité
comme une vulnérabilité de sécurité.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 7.0.56-3+really7.0.99-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2077.data"
# $Id: $
