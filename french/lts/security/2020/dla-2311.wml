#use wml::debian::translation-check translation="2c403204fd18cf33ec36cea4480935e2abad3a38" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle de script
intersite à l’aide d’éléments HTML <tt>iframe</tt> dans 
<a href="https://www.zabbix.com/">Zabbix</a>, un système de supervision basé sur
 PHP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15803">CVE-2020-15803</a>

<p>Zabbix avant les versions 3.0.32rc1, 4.x avant 4.0.22rc1, 4.1.x jusqu’à 4.4.x
avant 4.4.10rc1, et 5.x avant 5.0.2rc1 permet de stoker un XSS dans un composant
graphique d’URL.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:3.0.7+dfsg-3+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zabbix.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2311.data"
# $Id: $
