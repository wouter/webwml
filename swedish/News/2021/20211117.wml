# Status: published
# $Rev$
#use wml::debian::translation-check translation="adf66158e3f67f0832327ddcff20eacc1a03c5c7"

<define-tag pagetitle>Uttalande om Daniel Pocock</define-tag>
<define-tag release_date>2021-11-17</define-tag>
#use wml::debian::news

<p>Debian är medveten om ett antal offentliga inlägg som gjorts om Debian och
dess gemenskapsmedlemmar på en rad webbplatser av en Daniel Pocock, som utger
sig för att vara en Debianutvecklare.</p>

<p>Herr Pocock är inte associerad med Debian. Han är varken Debianutvecklare
eller en medlem av Debiangemenskapen. Han var tidigare Debianutvecklare men
uteslöts ur projektet för några år sedan efter att ha ägnat sig åt beteende
som var destruktivt för Debians rykte och för själva gemenskapen. Han har inte
varit medlem i Debianprojektet sedan 2018. Han är också förbjuden att delta i
Debians gemenskap i någon form, inklusive genom tekniska bidrag, deltagande
i onlineutrymmen, eller delta i konferenser och/eller evenemang. Han har ingen
rätt eller ställning att representera sig själv som Debianutvecklare eller
medlem av Debiangemenskapen.</p>

<p>Under tiden sedan han uteslöts ur projektet har herr Pocock engagerat sig i
en pågående och omfattande kampanj av vedergällningstrakasserier genom att göra
ett antal inflammatoriska och ärekränkande inlägg online, särskilt på en
webbplats som utger sig för att vara en Debianwebbplats. Innehållet i dessa
inlägg involverar inte bara Debian, utan också ett antal av dess utvecklare
och volontärer. Han har också forsatt att felaktigt framställa sig som medlem
i Debiangemenskapen i mycket av sin kommunikation och offentliga
presentationer. Rättsliga åtgärder övervägs för bland annat ärekränkning,
uppsåtlig lögn och trakasserier.</p>

<p>Debian står tillsammans som en gemenskap och mot trakasserier. Vi har en
uppförandekod som styr vårt svar på skadligt beteende i vår gemenskap, och vi
kommer att fortsätta att agera för att skydda vår gemenskap och våra
volontärer. Tveka inte att kontakta Debians communitygrupp om du har frågor
eller behöver support. Under tiden är alla Debians och voluntärers rättigheter
reserverade.


<h2>Om Debian</h2>

<p>
	Debianprojektet är en sammanslutning av utvecklare av fri mjukvara som
	ger frivilligt av sin tid och insats för att producera det helt fria
	operativsystemet Debian.
</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, var vänlig besök Debians webbplats på
<a href="$(HOME)/">http://www.debian.org/</a> eller skicka e-post till
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
