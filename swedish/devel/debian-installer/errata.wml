#use wml::debian::template title="Debian-Installer errata"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="50ddc8fab8f8142c1e8266a7c0c741f9bfe1b23a" mindelta="1" maxdelta="1"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<h1>Kända problem i <humanversion /></h1>

<p>
Detta är en lista på kända problem i utgåvan <humanversion /> av
Debian Installer. Om du inte ser ditt problem i listan här, vänligen sänd in en
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">installationsrapport</a>
som beskriver problemet.
</p>

<dl class="gloss">

    <dt>Trasigt räddningsläge med den grafiska installeraren</dt>

    <dd>Det har upptäckts under avbildningstestning för Bullseye RC 1 att
    räddningsläget verkar vara trasigt (<a href="https://bugs.debian.org/987377">#987377</a>).
    Utöver detta behöver räddningsetiketten i bannern justeras för Bullseye-temat.
    <br />
    <b>Status:</b> Rättat i Bullseye RC 2.</dd>

    <dt>amdgpu fastprogramvara (firmware) krävs för många AMD-grafikkort</dt>
    <dd>Det verkar finnas ett ökat behov att installera amdgpu-fastprogramvara
    (firmware) (genom det icke-fria paketet <code>firmware-amd-graphics</code>)
    för att undvika en svart skärm vid uppstart av det installerade systemet.
    Med Bullseye RC1, och även med en installationsavbildning som inkluderar
    alla firmwarepaket detekterar inte installeraren behovet av den specifika
    komponenten. Se
    <a href="https://bugs.debian.org/989863">paraplyfelrapporten</a>
    för att spåra våra insatser.

    <br />
    <b>Status:</b> Rättat i Bullseye RC 3.</dd>

 
    <dt>Fastprogramvara (firmware) krävs för vissa ljudkort</dt>
    <dd>Det verkar finnas ett antal ljudkort som kräver att en
    fastprogramvara (firmware) laddas för att leverera ljud. I Bullseye
    har inte installeraren möjligheten att ladda dem tidigt, vilket betyder
    att talsyntesen under installation inte är möjlig med sådana ljudkort.
    En möjlig workaround är att sätta in ett annat ljudkort som inte behöver
    sådan fastprogramvara.
    Se <a href="https://bugs.debian.org/992699">paraplyfelrapporten</a>
    för att spåra våra insatser.</dd>

	<dt>Skrivbordsinstallationer fungerar möjligen inte med hjälp av endast CD#1</dt>
	<dd>
		Tack vare utrymmesbrist på den första CDn, så får inte alla väntade delar
		av GNOME-skrivbordet plats på CD#1. Använd extra paket-källor (t.ex. en
		andra CD eller en nätverksspegel) för en framgångsrik installation,
		<br />
		<b>Status:</b> Det är osannolikt att fler insatser kommer göras för att få
		fler paket att få plats på CD#1.
	</dd>

	<dt>LUKS2 är inkompatibelt med GRUB's kryptodiskstöd</dt>
	<dd>Det upptäckts bara nyligen att GRUB inte har stöd för
		LUKS2. Detta betyder att användare som vill använda
		<tt>GRUB_ENABLE_CRYPTODISK</tt> och undvika en separat,
		icke krypterad <tt>/boot</tt>, inte kommer att göra detta
		(<a href="https://bugs.debian.org/927165">#927165</a>).
		Denna setup stöds inte i installeraren hursomhelst, men det
		vore förnuftigt att åtminstone dokumentera denna begränsning
		mer framträdande, och åtminstone ha möjligheten att välja
		LUKS1 under installationsprocessen.
	<br />
	<b>Status:</b> Några idéer har uttryckts i felrapporten. De ansvariga för
   cryptsetup har skrivit <a 
   href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">specifik
   dokumentation</a>.</dd>


</dl>
