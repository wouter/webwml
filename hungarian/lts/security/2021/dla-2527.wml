#use wml::debian::translation-check translation="0f8c1a26bd18c924370ac4b66154d37b8abfc80f" maintainer="Szabolcs Siebenhofer"
<define-tag description>LTS biztonsági frissítés</define-tag>
<define-tag moreinfo>
<p>A golang-go.crypto nemrégiben frissítve lett a <a href="https://security-tracker.debian.org/tracker/CVE-2019-11840">CVE-2019-11840</a>
sérülékenység miatt. Ahhoz, hogy a biztonsági javítás érvényre kerüljön, minden csomag újrafordítása szükséges.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11840">CVE-2019-11840</a>

    <p>Egy hibát fedeztek fel a Go kriptográfiai könyvtárakban, más néven a 
    golang-googlecode-go-crypto-ban. Amennyiben 256 GiB-ot meghaladó kulcsfolyam
    generálódott, vagy a számláló 32 bit-nél nagyobbra nőtt, az amd64 implementáció
    először inkorrekt kimenetet generált, majd visszatért a korábban használt 
    kulcsfolyamhoz. Az ismétlődő kulcsfolyam bájtok a bizalmasság elvesztéséhez
    vezethettek titkosító alkalmazásokban, vagy megjósolhatóvá váltak CSPRNG
    alkalmazásokban.</p></li>

</ul>

<p>A Debian 9 <q>stretch</q> esetén a probléma a 2.21-2+deb9u1 verzióban 
javításra került.</p>

<p>Azt tanácsoljuk, hogy frissítsd a snapd csomagjaidat.</p>

<p>A snapd csomag biztonság állapotával kapcsolatban lásd a bizonsági 
nyomkövető oldalát:
<a href="https://security-tracker.debian.org/tracker/snapd">https://security-tracker.debian.org/tracker/snapd</a></p>

<p>További információk a Debian LTS biztonági figyelmeztetéseiről, hogyan tudod ezeket a 
frissítéseket a rendszereden telepíteni és más gyakran feltett kérdések megtalálhatóak itt: 
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2527.data"
# $Id: $
