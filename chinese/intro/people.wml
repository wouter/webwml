#use wml::debian::template title="参与者们：我们是谁，我们做些什么" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

# translators: some text is taken from /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#history">一切从何开始<a>
    <li><a href="#devcont">开发者与贡献者</a>
    <li><a href="#supporters">支持 Debian 的个人和组织</a>
    <li><a href="#users">Debian 用户</a>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>因为有很多人问过，所以在此解答一下：Debian 的发音是 <span style="white-space: nowrap;">/&#712;de.bi.&#601;n/。</span>它来自于 Debian 的创造者 Ian Murdock 和他的妻子 Debra 的名字。</p>
</aside>

<h2><a id="history">一切从何开始</a></h2>

<p>在 1993 年 8 月，Ian Murdock 开始着手创建一个符合 Linux 和 GNU 精神的、
开放的操作系统。他向其他软件开发人员发出了一份公开邀请，希望他们
为一个基于 Linux 内核的发行版作出贡献，而 Linux 内核在当时
还很年轻。Debian 的设计目标是让各组件能够精巧地结合在一起，并
得到仔细的维护和支持，同时接受来自自由软件社区的开放设计、贡献和支持。</p>

<p>它最初是一个由自由软件极客组成的相处融洽的小团体，后
来逐渐发展成为一个由开发人员、贡献者和用户组成的组织良好的大型社区。</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="$(DOC)/manuals/project-history/">阅读完整的历史</a></button></p>

<h2><a id="devcont">开发者与贡献者</a></h2>

<p>
Debian 是一个完全由志愿者构成的组织。
分布在<a href="$(DEVEL)/developers.loc">世界各地</a>的超过一千名
活跃开发人员在闲暇时间自愿开发 Debian。
很少有开发者真的见过面。
通信主要通过电子邮件
（邮件列表参见 <a href="https://lists.debian.org/">lists.debian.org</a>）
和 IRC（irc.debian.org 的 #debian 频道）进行。
</p>

<p>
官方 Debian 成员的完整列表可在 
<a href="https://nm.debian.org/members">nm.debian.org</a> 找到。
为 Debian 发行版工作过的贡献者和团队的完整列表请见
<a href="https://contributors.debian.org">contributors.debian.org</a>。
</p>

<p>Debian 项目拥有一个精心组织的<a href="organization">组织架构</a>。
有关 Debian 内部的更多信息，请浏览<a href="$(DEVEL)/">开发者天地</a>。</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="philosophy">了解我们的理念</a></button></p>

<h2><a id="supporters">支持 Debian 的个人和组织</a></h2>

<p>除了开发者和贡献者，许多其他的个人和组织都是 Debian 社区的一部分：</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">主机和硬件资助者</a></li>
  <li><a href="../mirror/sponsors">镜像站资助者</a></li>
  <li><a href="../partners/">开发和服务伙伴</a></li>
  <li><a href="../consultants">顾问</a></li>
  <li><a href="../CD/vendors">Debian 安装介质供应商</a></li>
  <li><a href="../distrib/pre-installed">预装 Debian 的计算机供应商</a></li>
  <li><a href="../events/merchandise">相关商品销售商</a></li>
</ul>

<h2><a id="users">Debian 用户</a></h2>

<p>
Debian 被各种各样的大小组织，以及成千上万的个人所使用。
阅读<a href="../users/">谁在使用 Debian？</a>页面以获取使用 Debian 的教育、
商业、非营利组织和政府部门的列表，
他们提交了关于如何以及为什么使用 Debian 的简短描述。
</p>
