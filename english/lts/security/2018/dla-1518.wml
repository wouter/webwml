<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two vulnerabilities were discovered in polarssl, a lightweight crypto and
SSL/TLS library (nowadays continued under the name mbedtls) which could
result in plain text recovery via side-channel attacks.</p>

<p>Two other minor vulnerabilities were discovered in polarssl which could
result in arithmetic overflow errors.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0497">CVE-2018-0497</a>

    <p>As a protection against the Lucky Thirteen attack, the TLS code for
    CBC decryption in encrypt-then-MAC mode performs extra MAC
    calculations to compensate for variations in message size due to
    padding. The amount of extra MAC calculation to perform was based on
    the assumption that the bulk of the time is spent in processing
    64-byte blocks, which is correct for most supported hashes but not for
    SHA-384. Correct the amount of extra work for SHA-384 (and SHA-512
    which is currently not used in TLS, and MD2 although no one should
    care about that).</p>

    <p>This is a regression fix for what <a href="https://security-tracker.debian.org/tracker/CVE-2013-0169">CVE-2013-0169</a> had been fixed this.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0498">CVE-2018-0498</a>

    <p>The basis for the Lucky 13 family of attacks is for an attacker to be
    able to distinguish between (long) valid TLS-CBC padding and invalid
    TLS-CBC padding. Since our code sets padlen = 0 for invalid padding,
    the length of the input to the HMAC function gives information about
    that.</p>

    <p>Information about this length (modulo the MD/SHA block size) can be
    deduced from how much MD/SHA padding (this is distinct from TLS-CBC
    padding) is used. If MD/SHA padding is read from a (static) buffer, a
    local attacker could get information about how much is used via a
    cache attack targeting that buffer.</p>

    <p>Let's get rid of this buffer. Now the only buffer used is the
    internal MD/SHA one, which is always read fully by the process()
    function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9988">CVE-2018-9988</a>

    <p>Prevent arithmetic overflow on bounds check and add bound check
    before signature length read in ssl_parse_server_key_exchange().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9989">CVE-2018-9989</a>

    <p>Prevent arithmetic overflow on bounds check and add bound check
    before length read in ssl_parse_server_psk_hint()</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.9-2.1+deb8u4.</p>

<p>We recommend that you upgrade your polarssl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1518.data"
# $Id: $
