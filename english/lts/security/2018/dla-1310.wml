<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Various issues were discovered in exempi, a library to parse XMP
metadata that may cause a denial-of-service or may have other
unspecified impact via crafted files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18233">CVE-2017-18233</a>

    <p>An Integer overflow in the Chunk class in RIFF.cpp allows remote
    attackers to cause a denial of service (infinite loop) via crafted
    XMP data in an .avi file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18234">CVE-2017-18234</a>

    <p>An issue was discovered that allows remote attackers to cause a
    denial of service (invalid memcpy with resultant use-after-free)
    or possibly have unspecified other impact via a .pdf file containing
    JPEG data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18236">CVE-2017-18236</a>

    <p>The ASF_Support::ReadHeaderObject function in ASF_Support.cpp allows
    remote attackers to cause a denial of service (infinite loop) via a
    crafted .asf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18238">CVE-2017-18238</a>

    <p>The TradQT_Manager::ParseCachedBoxes function in
    QuickTime_Support.cpp allows remote attackers to cause
    a denial of service (infinite loop) via crafted XMP data in
    a .qt file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7728">CVE-2018-7728</a>

   <p>TIFF_Handler.cpp mishandles a case of a zero length, leading to a
   heap-based buffer over-read in the MD5Update() function in
   MD5.cpp.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7730">CVE-2018-7730</a>

    <p>A certain case of a 0xffffffff length is mishandled in
    PSIR_FileWriter.cpp, leading to a heap-based buffer over-read
    in the PSD_MetaHandler::CacheFileData() function.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.2.0-1+deb7u1.</p>

<p>We recommend that you upgrade your exempi packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1310.data"
# $Id: $
