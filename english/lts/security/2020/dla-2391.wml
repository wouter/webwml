<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A potential HTTP request smuggling vulnerability in WEBrick
was reported.</p>

<p>WEBrick (bundled along with ruby2.3) was too tolerant against
an invalid Transfer-Encoding header. This may lead to
inconsistent interpretation between WEBrick and some HTTP proxy
servers, which may allow the attacker to “smuggle” a request.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.3.3-1+deb9u9.</p>

<p>We recommend that you upgrade your ruby2.3 packages.</p>

<p>For the detailed security status of ruby2.3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby2.3">https://security-tracker.debian.org/tracker/ruby2.3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2391.data"
# $Id: $
