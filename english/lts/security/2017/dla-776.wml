<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Simo Sorce of Red Hat discovered that the Samba client code always
requests a forwardable ticket when using Kerberos authentication. A
target server, which must be in the current or trusted domain/realm,
is given a valid general purpose Kerberos <q>Ticket Granting Ticket</q>
(TGT), which can be used to fully impersonate the authenticated user
or service.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:3.6.6-6+deb7u11.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-776.data"
# $Id: $
