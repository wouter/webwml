<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Toni Huttunen discovered that the Shibboleth service provider's template
engine used to render error pages could be abused for phishing attacks.</p>

<p>For additional information please refer to the upstream advisory at
<a href="https://shibboleth.net/community/advisories/secadv_20210317.txt">https://shibboleth.net/community/advisories/secadv_20210317.txt</a></p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.6.0+dfsg1-4+deb9u2.</p>

<p>We recommend that you upgrade your shibboleth-sp2 packages.</p>

<p>For the detailed security status of shibboleth-sp2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/shibboleth-sp2">https://security-tracker.debian.org/tracker/shibboleth-sp2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2599.data"
# $Id: $
