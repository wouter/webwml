<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Phpmyadmin, a web administration tool for MySQL, had several
vulnerabilities reported.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6606">CVE-2016-6606</a>

    <p>A pair of vulnerabilities were found affecting the way cookies are
    stored.</p>

    <p>The decryption of the username/password is vulnerable to a padding
    oracle attack. The can allow an attacker who has access to a user's
    browser cookie file to decrypt the username and password.</p>

    <p>A vulnerability was found where the same initialization vector
    is used to hash the username and password stored in the phpMyAdmin
    cookie. If a user has the same password as their username, an
    attacker who examines the browser cookie can see that they are the
    same — but the attacker can not directly decode these values from
    the cookie as it is still hashed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6607">CVE-2016-6607</a>

    <p>Cross site scripting vulnerability in the replication feature</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6609">CVE-2016-6609</a>

    <p>A specially crafted database name could be used to run arbitrary PHP
    commands through the array export feature.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6611">CVE-2016-6611</a>

    <p>A specially crafted database and/or table name can be used to trigger
    an SQL injection attack through the SQL export functionality.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6612">CVE-2016-6612</a>

    <p>A user can exploit the LOAD LOCAL INFILE functionality to expose
    files on the server to the database system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6613">CVE-2016-6613</a>

    <p>A user can specially craft a symlink on disk, to a file which
    phpMyAdmin is permitted to read but the user is not, which
    phpMyAdmin will then expose to the user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6614">CVE-2016-6614</a>

    <p>A vulnerability was reported with the %u username replacement
    functionality of the SaveDir and UploadDir features. When the
    username substitution is configured, a specially-crafted user name
    can be used to circumvent restrictions to traverse the file system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6620">CVE-2016-6620</a>

    <p>A vulnerability was reported where some data is passed to the PHP
    unserialize() function without verification that it's valid
    serialized data. Due to how the PHP function operates,
    unserialization can result in code being loaded and executed due to
    object instantiation and autoloading, and a malicious user may be
    able to exploit this.
    Therefore, a malicious user may be able to manipulate the stored
    data in a way to exploit this weakness.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6622">CVE-2016-6622</a>

    <p>An unauthenticated user is able to execute a denial-of-service
    attack by forcing persistent connections when phpMyAdmin is running
    with $cfg['AllowArbitraryServer']=true;.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6623">CVE-2016-6623</a>

    <p>A malicious authorized user can cause a denial-of-service attack
    on a server by passing large values to a loop.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6624">CVE-2016-6624</a>

    <p>A vulnerability was discovered where, under certain circumstances,
    it may be possible to circumvent the phpMyAdmin IP-based
    authentication rules.
    When phpMyAdmin is used with IPv6 in a proxy server environment,
    and the proxy server is in the allowed range but the attacking
    computer is not allowed, this vulnerability can allow the attacking
    computer to connect despite the IP rules.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6630">CVE-2016-6630</a>

    <p>An authenticated user can trigger a denial-of-service attack by
    entering a very long password at the change password dialog.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6631">CVE-2016-6631</a>

    <p>A vulnerability was discovered where a user can execute a remote
    code execution attack against a server when phpMyAdmin is being
    run as a CGI application. Under certain server configurations,
    a user can pass a query string which is executed as a
    command-line argument by shell scripts.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.4.11.1-2+deb7u6.</p>

<p>We recommend that you upgrade your phpmyadmin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-626.data"
# $Id: $
