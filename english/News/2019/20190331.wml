<define-tag pagetitle>The Debian Project mourns the loss of Innocent de Marchi </define-tag>
<define-tag release_date>2019-03-31</define-tag>
#use wml::debian::news
# $Id$

<p>The Debian Project recently learned that it has lost a member of its 
community. Innocent de Marchi passed a few months ago.</p>

<p>
Innocent was a math teacher and a free software developer. One of his passions 
was tangram puzzles, which led him to write a tangram-like game that he later
packaged and maintained in Debian. Soon his contributions expanded to other
areas, and he also worked as a tireless translator into Catalan.
</p>

<p>The Debian Project honors his good work and strong dedication to Debian
and Free Software. Innocent's contributions will not be forgotten, and the
high standards of his work will continue to serve as an inspiration to
others.</p>

<h2>About Debian</h2>
<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely free
operating system Debian.</p>

<h2>Contact Information</h2>
<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
