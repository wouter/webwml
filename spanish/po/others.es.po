# Traducción del sitio web de Debian
# Copyright (C) 2004 SPI Inc.
# Javier Fernández-Sanguino <jfs@debian.org>, 2004
#
msgid ""
msgstr ""
"Project-Id-Version: Others webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-01-24 13:06+0100\n"
"Last-Translator: Laura Arjona Reina <larjona@debian.org>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "El rincón de los nuevos miembros"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Paso 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Paso 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Paso 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Paso 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Paso 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Paso 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Paso 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Lista de solicitantes"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Vea <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(sólo en francés) si desea más información."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "Más información"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Vea <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/"
"coordinacion.es.html</a> (solo en español) si desea más información."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Teléfono"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Fax"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Dirección"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Productos"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "Camisetas"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "gorras"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "pegatinas"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "tazas"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "otras ropas"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "polos"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "discos voladores"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "alfombrilla de ratón"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "emblemas"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "canasta de baloncesto"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "pendientes"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "maletines"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "paraguas"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "almohadas"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "llaveros"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "navajas suizas"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "memorias USB"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "Cintas «lanyard»"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "otros"

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr "Idiomas disponibles:"

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr "Envíos internacionales:"

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr "en Europa"

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr "País de origen:"

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr "Dona dinero a Debian"

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr "El dinero se utiliza para organizar actos locales sobre software libre"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Con&nbsp;«Debian»"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Sin&nbsp;«Debian»"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "PostScript encapsulado (EPS)"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Funciona con Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Funciona con Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Funciona con Debian]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (mini botón)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "igual que el anterior"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "¿Desde hace cuánto tiempo utilizas Debian?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "¿Eres Desarrolladora de Debian?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "¿En qué áreas de Debian estás involucrada?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "¿Qué te hizo interesarte en trabajar con Debian?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""
"¿Tienes alguna recomendación para mujeres que estén interesadas en "
"participar más en Debian?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""
"¿Estás involucrada en algún grupo de tecnología de mujeres? ¿En cuál/es?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Cuéntanos algo más sobre ti..."

#~ msgid "Working"
#~ msgstr "Funciona"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (roto)"

#~ msgid "Booting"
#~ msgstr "Arranca"

#~ msgid "Building"
#~ msgstr "Compila"

#~ msgid "Not yet"
#~ msgstr "Aún no"

#~ msgid "No kernel"
#~ msgstr "Sin núcleo"

#~ msgid "No images"
#~ msgstr "Sin imágenes"

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "<void id=\"d-i\" />Desconocido"

#~ msgid "Unavailable"
#~ msgstr "No disponible"

#~ msgid "Download"
#~ msgstr "Descargar"

#~ msgid "Old banner ads"
#~ msgstr "Pancartas antiguas"

#~ msgid "OK"
#~ msgstr "Bien"

#~ msgid "BAD"
#~ msgstr "Mal"

#~ msgid "OK?"
#~ msgstr "¿Bien?"

#~ msgid "BAD?"
#~ msgstr "¿Mal?"

#~ msgid "??"
#~ msgstr "¿?"

#~ msgid "Unknown"
#~ msgstr "Se desconoce"

#~ msgid "ALL"
#~ msgstr "Todos"

#~ msgid "Package"
#~ msgstr "Paquete"

#~ msgid "Status"
#~ msgstr "Estado"

#~ msgid "Version"
#~ msgstr "Versión"

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "Last update"
#~ msgstr "Última actualización"

#~ msgid "Wanted:"
#~ msgstr "Deseado:"

#~ msgid "Who:"
#~ msgstr "Quién:"

#~ msgid "Architecture:"
#~ msgstr "Arquitectura"

#~ msgid "Specifications:"
#~ msgstr "Especificaciones:"

#~ msgid "Where:"
#~ msgstr "Dónde:"

#~ msgid "Name:"
#~ msgstr "Nombre:"

#~ msgid "Company:"
#~ msgstr "Compañía:"

#~ msgid "URL:"
#~ msgstr "URL:"

#~ msgid "or"
#~ msgstr "o"

#~ msgid "Email:"
#~ msgstr "Correo electrónico:"

#~ msgid "Rates:"
#~ msgstr "Tarifas:"

#~ msgid "Willing to Relocate"
#~ msgstr "Sin localización definida"

#~ msgid ""
#~ "<total_consultant> Debian consultants listed in <total_country> countries "
#~ "worldwide."
#~ msgstr ""
#~ "<total_consultant> consultores de Debian en total en <total_country> "
#~ "países por todo el mundo."

#~ msgid "Mailing List Subscription"
#~ msgstr "Suscripción a las listas de correo"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription "
#~ "web form</a> is also available, for unsubscribing from mailing lists. "
#~ msgstr ""
#~ "Vea la página de las <a href=\"./#subunsub\">listas de correo</a> para "
#~ "tener información sobre cómo suscribirse usando el correo electrónico. "
#~ "También hay disponible un <a href=\"unsubscribe\">formulario web de "
#~ "desuscripción</a>, para darse de baja de las listas de correo. "

#~ msgid ""
#~ "Note that most Debian mailing lists are public forums. Any mails sent to "
#~ "them will be published in public mailing list archives and indexed by "
#~ "search engines. You should only subscribe to Debian mailing lists using "
#~ "an e-mail address that you do not mind being made public."
#~ msgstr ""
#~ "Tenga en cuenta que la mayoría de las listas de correo de Debian son "
#~ "foros públicos. Cualquier correo enviado a las listas se publicará en los "
#~ "archivos de las listas de correo y será indexado por motores de búsqueda. "
#~ "Sólo debería suscribirse a las listas de correo de Debian con una "
#~ "dirección de correo que no le importa que se haga pública."

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Por favor, seleccione a qué listas quiere suscribirse:"

#~ msgid "No description given"
#~ msgstr "Sin descripción."

#~ msgid "Moderated:"
#~ msgstr "Moderada:"

#~ msgid "Posting messages allowed only to subscribers."
#~ msgstr "El envío de mensajes sólo está permitido a los suscritos."

#~ msgid ""
#~ "Only messages signed by a Debian developer will be accepted by this list."
#~ msgstr ""
#~ "Sólo se aceptarán mensajes a esta lista si están firmados por un "
#~ "desarrollador de Debian."

#~ msgid "Subscription:"
#~ msgstr "Suscripción:"

#~ msgid "is a read-only, digestified version."
#~ msgstr "es una versión resumida, de sólo lectura."

#~ msgid "Your E-Mail address:"
#~ msgstr "Su dirección de correo:"

#~ msgid "Subscribe"
#~ msgstr "Enviar suscripción"

#~ msgid "Clear"
#~ msgstr "Borrar selección"

#~ msgid ""
#~ "Please respect the <a href=\"./#ads\">Debian mailing list advertising "
#~ "policy</a>."
#~ msgstr ""
#~ " Por favor, respete la <a href=\"./#ads\">política de publicidad en las "
#~ "listas de correo de Debian</a>. "

#~ msgid "Mailing List Unsubscription"
#~ msgstr "Desuscripción de las listas de correo"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription "
#~ "web form</a> is also available, for subscribing to mailing lists. "
#~ msgstr ""
#~ "Vea la página de las <a href=\"./#subunsub\">listas de correo</a> para "
#~ "tener información sobre cómo desuscribirse usando el correo electrónico. "
#~ "También hay disponible un <a href=\"subscribe\">formulario web de "
#~ "suscripción</a>, para suscribirse a las listas de correo. "

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr "Por favor, seleccione las listas de las que quiere darse de baja:"

#~ msgid "Unsubscribe"
#~ msgstr "Enviar desuscripción"

#~ msgid "open"
#~ msgstr "abierta"

#~ msgid "closed"
#~ msgstr "cerrada"

#~ msgid "beta 4"
#~ msgstr "beta 4"

#~ msgid "Debian Technical Committee only"
#~ msgstr "sólo Comité Técnico de Debian"

#~ msgid "deity developers only"
#~ msgstr "sólo desarrolladores de deity"

#~ msgid "developers only"
#~ msgstr "sólo desarrolladores"

#~ msgid "Q"
#~ msgstr "P"

#~ msgid ""
#~ "(<a href=\"http://www.debian-br.org/projetos/webwml.php\">more info</a>)"
#~ msgstr ""
#~ "(<a href=\"http://www.debian-br.org/projetos/webwml.php\">más "
#~ "información</a>)"

#~ msgid "(<a href=\"m4_HOME/intl/Swedish/bidragslamnare#web\">more info</a>)"
#~ msgstr ""
#~ "(<a href=\"m4_HOME/intl/Swedish/bidragslamnare#web\">más información</a>)"

#~ msgid "vertical&nbsp;banner"
#~ msgstr "cartel&nbsp;vertical"

#~ msgid "horizontal&nbsp;banner"
#~ msgstr "cartel&nbsp;horizontal"

#~ msgid "animated&nbsp;GIF&nbsp;banner"
#~ msgstr "cartel&nbsp;GIF&nbsp;animado"

#~ msgid "Newer Debian banners"
#~ msgstr "Nuevas pancartas de Debian"

#~ msgid "Debian: apt-get into it."
#~ msgstr "Debian: apt-get'étate dentro de ella."

#~ msgid "Debian: the potato has landed. announcing version 2.2"
#~ msgstr "Debian: la patata ha aterrizado. Anunciando la versión 2.2"

#~ msgid "Older Debian banners"
#~ msgstr "Pancartas de Debian antiguas"

#~ msgid "Debian: The biggest is still the best."
#~ msgstr "Debian: Lo más grande aún es lo mejor."

#~ msgid "Debian: The Perfect OS"
#~ msgstr "Debian: El S.O perfecto."

#~ msgid "Debian GNU/Linux: Your next Linux distribution."
#~ msgstr "Debian GNU/Linux: Tu próxima distribución de Linux."

#~ msgid "rc2"
#~ msgstr "rc2"

#~ msgid "rc3"
#~ msgstr "rc3"

#~ msgid "rc3 (broken)"
#~ msgstr "rc3 (roto)"

#~ msgid "Topics:"
#~ msgstr "Temas:"

#~ msgid "Location:"
#~ msgstr "Localización:"

#~ msgid "Previous Talks:"
#~ msgstr "Conferencias previas:"

#~ msgid "p<get-var page />"
#~ msgstr "pág.<get-var page />"
