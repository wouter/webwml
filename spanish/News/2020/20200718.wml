#use wml::debian::translation-check translation="b51002b9cd5b7dbca0e39ddb2d17bcc495ead21a"
<define-tag pagetitle>Debian 9 actualizado: publicada la versión 9.13</define-tag>
<define-tag release_date>2020-07-18</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.13</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la decimotercera (y última) actualización de su
distribución «antigua estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tras la publicación de esta versión, los equipos de seguridad y responsable de la publicación de Debian ya no
proporcionarán actualizaciones para Debian 9. Los usuarios que deseen continuar recibiendo soporte de seguridad
deberían actualizar a Debian 10 o consultar <url "https://wiki.debian.org/LTS"> para detalles
sobre el subconjunto de arquitecturas y paquetes que están cubiertos por el proyecto de soporte a largo
plazo («LTS»).</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «antigua estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction acmetool "Recompilado contra golang reciente para recoger correcciones de seguridad">
<correction atril "dvi: mitiga ataques de inyección de órdenes cambiando la manera de entrecomillar el nombre de fichero [CVE-2017-1000159]; corrige comprobaciones de desbordamiento en backend tiff [CVE-2019-1010006]; tiff: gestiona fallo procedente de TIFFReadRGBAImageOriented [CVE-2019-11459]">
<correction bacula "Añade paquete de transición bacula-director-common, evitando la pérdida de /etc/bacula/bacula-dir.conf al eliminar el paquete («purge»); hace que root sea el propietario de los ficheros de PID">
<correction base-files "Actualiza /etc/debian_version para esta versión">
<correction batik "Corrige falsificación de solicitudes en el lado servidor («server-side request forgery») mediante atributos xlink:href [CVE-2019-17566]">
<correction c-icap-modules "Soporta ClamAV 0.102">
<correction ca-certificates "Actualiza el lote de CA de Mozilla a la 2.40, añade a la lista negra los no confiables certificados raíz de Symantec y el expirado <q>AddTrust External Root</q>; elimina certificados exclusivos para correo electrónico">
<correction chasquid "Recompilado contra golang reciente para recoger correcciones de seguridad">
<correction checkstyle "Corrige problema de inyección de entidad externa XML [CVE-2019-9658 CVE-2019-10782]">
<correction clamav "Nueva versión del proyecto original [CVE-2020-3123]; correcciones de seguridad [CVE-2020-3327 CVE-2020-3341]">
<correction compactheader "Nueva versión del proyecto original, compatible con versiones más recientes de Thunderbird">
<correction cram "Ignora fallos de pruebas para corregir problemas de compilación">
<correction csync2 "Falla la orden HELLO cuando se requiere SSL">
<correction cups "Corrige desbordamiento de memoria dinámica («heap») [CVE-2020-3898] y <q>la función `ippReadIO` puede leer fuera de límites un campo extensión ('under-read an extension field')</q> [CVE-2019-8842]">
<correction dbus "Nueva versión «estable» del proyecto original; evita un problema de denegación de servicio [CVE-2020-12049]; evita «uso tras liberar» si dos nombres de usuario tienen el mismo uid">
<correction debian-installer "Actualizado para la ABI del núcleo Linux 4.9.0-13">
<correction debian-installer-netboot-images "Recompilado contra stretch-proposed-updates">
<correction debian-security-support "Actualiza el estado de soporte de varios paquetes">
<correction erlang "Corrige el uso de algoritmos de cifrado TLS débiles [CVE-2020-12872]">
<correction exiv2 "Corrige problema de denegación de servicio [CVE-2018-16336]; corrige solución demasiado restrictiva para CVE-2018-10958 y CVE-2018-10999">
<correction fex "Actualización de seguridad">
<correction file-roller "Corrección de seguridad [CVE-2020-11736]">
<correction fwupd "Nueva versión del proyecto original; usa un CNAME para redireccionar al CDN correcto para los metadatos; no aborta el arranque si el fichero XML de metadatos es inválido; añade las claves públicas GPG de la Linux Foundation para firmware y metadatos; aumenta el límite de metadatos a 10MB">
<correction glib-networking "Devuelve error de identidad errónea si identity no tiene valor [CVE-2020-13645]">
<correction gnutls28 "Corrige problema de corrupción de memoria [CVE-2019-3829]; corrige filtración de contenido de la memoria; añade soporte para tickets de sesión de longitud cero, corrige errores de conexión a algunos proveedores de alojamiento («hosting providers») en sesiones TLS1.2">
<correction gosa "Hace más stricta la comprobación de LDAP correcto/erróneo [CVE-2019-11187]; corrige compatibilidad con versiones de PHP más recientes; retroadapta otros parches; sustituye (un)serialize por json_encode/json_decode para mitigar inyección de objetos PHP [CVE-2019-14466]">
<correction heartbleeder "Recompilado contra golang reciente para recoger correcciones de seguridad">
<correction intel-microcode "Devuelve algunos microcódigos a versiones previas, como solución provisional a cuelgues durante el arranque en Skylake-U/Y y Skylake Xeon E3">
<correction iptables-persistent "No falla en caso de que lo haga modprobe">
<correction jackson-databind "Corrige varios problemas de seguridad que afectan a BeanDeserializerFactory [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840 CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620 CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969 CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 and CVE-2019-17267]">
<correction libbusiness-hours-perl "Usa cuatro dígitos para los años, corrigiendo problemas de compilación y de uso">
<correction libclamunrar "Nueva versión «estable» del proyecto original; añade un metapaquete sin número de versión">
<correction libdbi "Marca de nuevo como comentario la llamada a _error_handler(), corrigiendo problemas con consumidores">
<correction libembperl-perl "Trata páginas de error de Apache &gt;= 2.4.40">
<correction libexif "Correcciones de seguridad [CVE-2016-6328 CVE-2017-7544 CVE-2018-20030 CVE-2020-12767 CVE-2020-0093]; correcciones de seguridad [CVE-2020-13112 CVE-2020-13113 CVE-2020-13114]; corrige una lectura de memoria fuera de límites [CVE-2020-0182] y un desbordamiento de entero sin signo [CVE-2020-0198]">
<correction libvncserver "Corrige desbordamiento de memoria dinámica («heap») [CVE-2019-15690]">
<correction linux "Nueva versión «estable» del proyecto original; actualiza la ABI a la 4.9.0-13">
<correction linux-latest "Actualizado para la ABI del núcleo 4.9.0-13">
<correction mariadb-10.1 "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2020-2752 CVE-2020-2812 CVE-2020-2814]">
<correction megatools "Añade soporte para el nuevo formato de los enlaces mega.nz">
<correction mod-gnutls "Evita conjuntos de algoritmos de cifrado discontinuados («deprecated») en el conjunto de pruebas; corrige fallos de pruebas cuando se combina con la corrección de Apache para CVE-2019-10092">
<correction mongo-tools "Recompilado contra golang reciente para recoger correcciones de seguridad">
<correction neon27 "Trata fallos de pruebas relacionados con OpenSSL como no fatales">
<correction nfs-utils "Corrige potencial vulnerabilidad de sobreescritura de fichero [CVE-2019-3689]; no hace que el propietario de todo /var/lib/nfs sea el usuario statd">
<correction nginx "Corrige vulnerabilidad de «contrabando» de petición («request smuggling») de páginas de error [CVE-2019-20372]">
<correction node-url-parse "Sanea rutas y nombres de máquina («hosts») antes del análisis sintáctico [CVE-2018-3774]">
<correction nvidia-graphics-drivers "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2020-5963 CVE-2020-5967]">
<correction pcl "Corrige dependencia con libvtk6-qt-dev, que faltaba">
<correction perl "Corrige varios problemas de seguridad relacionados con expresiones regulares [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde "Corrige vulnerabilidad de ejecución de scripts entre sitios («cross-site scripting») [CVE-2020-8035]">
<correction php-horde-data "Corrige vulnerabilidad de ejecución de código remoto por usuarios autenticados [CVE-2020-8518]">
<correction php-horde-form "Corrige vulnerabilidad de ejecución de código remoto por usuarios autenticados [CVE-2020-8866]">
<correction php-horde-gollem "Corrige vulnerabilidad de ejecución de scripts entre sitios («cross-site scripting») en la salida de breadcrumb [CVE-2020-8034]">
<correction php-horde-trean "Corrige vulnerabilidad de ejecución de código remoto por usuarios autenticados [CVE-2020-8865]">
<correction phpmyadmin "Varias correcciones de seguridad [CVE-2018-19968 CVE-2018-19970 CVE-2018-7260 CVE-2019-11768 CVE-2019-12616 CVE-2019-6798 CVE-2019-6799 CVE-2020-10802 CVE-2020-10803 CVE-2020-10804 CVE-2020-5504]">
<correction postfix "Nueva versión «estable» del proyecto original">
<correction proftpd-dfsg "Corrige gestión de paquetes SSH_MSG_IGNORE">
<correction python-icalendar "Corrige dependencias con Python3">
<correction rails "Corrige posible ejecución de scripts entre sitios («cross-site scripting») mediante métodos de ayuda Javascript para codificar caracteres y evitar su evaluación («escape helper») [CVE-2020-5267]">
<correction rake "Corrige vulnerabilidad de inyección de órdenes [CVE-2020-8130]">
<correction roundcube "Corrige problema de ejecución de scripts entre sitios («cross-site scripting») mediante mensajes HTML con svg/namespace malicioso [CVE-2020-15562]">
<correction ruby-json "Corrige vulnerabilidad de creación insegura de objetos [CVE-2020-10663]">
<correction ruby2.3 "Corrige vulnerabilidad de creación insegura de objetos [CVE-2020-10663]">
<correction sendmail "Corrige la localización del proceso de control de los mensajeros de las colas («queue runner») en modo <q>split daemon</q> («daemons separados»), el texto del mensaje <q>NOQUEUE: connect from (null)</q>, fallo de eliminación cuando se utiliza BTRFS">
<correction sogo-connector "Nueva versión del proyecto original, compatible con versiones más recientes de Thunderbird">
<correction ssvnc "Corrige escritura fuera de límites [CVE-2018-20020], bucle infinito [CVE-2018-20021], inicialización errónea [CVE-2018-20022], potencial denegación de servicio [CVE-2018-20024]">
<correction storebackup "Corrige posible vulnerabilidad de elevación de privilegios [CVE-2020-7040]">
<correction swt-gtk "Corrige dependencia con libwebkitgtk-1.0-0, que faltaba">
<correction tinyproxy "Crea fichero de PID antes de reducir privilegios a los de una cuenta distinta de root [CVE-2017-11747]">
<correction tzdata "Nueva versión «estable» del proyecto original">
<correction websockify "Corrige dependencia con python{3,}-pkg-resources, que faltaba">
<correction wpa "Corrige elusión de la protección contra la desconexión con PMF en modo AP [CVE-2019-16275]; corrige problemas de aleatorización de la MAC con algunas tarjetas">
<correction xdg-utils "Sanea el nombre de la ventana antes de enviárselo a D-Bus; gestiona correctamente directorios cuyo nombre contiene espacios; crea el directorio <q>applications</q> si es necesario">
<correction xml-security-c "Corrige el cálculo de una longitud en el método concat">
<correction xtrlock "Corrige el bloqueo de (algunos) dispositivos multitáctiles mientras está cerrado [CVE-2016-10894]">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «antigua estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2017 4005 openjfx>
<dsa 2018 4255 ant>
<dsa 2018 4352 chromium-browser>
<dsa 2019 4379 golang-1.7>
<dsa 2019 4380 golang-1.8>
<dsa 2019 4395 chromium>
<dsa 2019 4421 chromium>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4621 openjdk-8>
<dsa 2020 4622 postgresql-9.6>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4628 php7.0>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4642 thunderbird>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4666 openldap>
<dsa 2020 4668 openjdk-8>
<dsa 2020 4670 tiff>
<dsa 2020 4671 vlc>
<dsa 2020 4673 tomcat8>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4683 thunderbird>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4692 netqmail>
<dsa 2020 4693 drupal7>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4698 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4703 mysql-connector-java>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4706 drupal7>
<dsa 2020 4707 mutt>
<dsa 2020 4711 coturn>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4715 imagemagick>
<dsa 2020 4717 php7.0>
<dsa 2020 4718 thunderbird>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction certificatepatrol "Incompatible con versiones más recientes de Firefox ESR">
<correction colorediffs-extension "Incompatible con versiones más recientes de Thunderbird">
<correction dynalogin "Depende de simpleid, que se elimina">
<correction enigmail "Incompatible con versiones más recientes de Thunderbird">
<correction firefox-esr "[armel] Deja de estar soportado (requiere nodejs)">
<correction firefox-esr "[mips mipsel mips64el] Deja de estar soportado (necesita rustc más reciente)">
<correction getlive "Roto por cambios en Hotmail">
<correction gplaycli "Roto por cambios en la API de Google">
<correction kerneloops "Ya no está disponible el servicio del proyecto original">
<correction libmicrodns "Problemas de seguridad">
<correction libperlspeak-perl "Problemas de seguridad; sin desarrollo activo">
<correction mathematica-fonts "Depende de una ubicación de descargas no disponible">
<correction pdns-recursor "Problemas de seguridad; no soportado">
<correction predictprotein "Depende de profphd, que se elimina">
<correction profphd "Inservible">
<correction quotecolors "Incompatible con versiones más recientes de Thunderbird">
<correction selenium-firefoxdriver "Incompatible con versiones más recientes de Firefox ESR">
<correction simpleid "No funciona con PHP7">
<correction simpleid-ldap "Depende de simpleid, que se elimina">
<correction torbirdy "Incompatible con versiones más recientes de Thunderbird">
<correction weboob "Sin desarrollo activo; ya eliminado de versiones posteriores">
<correction yahoo2mbox "Roto desde hace varios años">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «antigua estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «antigua estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Actualizaciones propuestas a la distribución «antigua estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Información sobre la distribución «antigua estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


