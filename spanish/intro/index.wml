#use wml::debian::template title="Introducción a Debian" MAINPAGE="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
<h2>Debian es una comunidad</h2>
<p>Miles de voluntarios en todo el mundo trabajan juntos en el sistema operativo Debian, priorizando el software libre. Conozca el proyecto Debian.</p>
    </div>

    <div style="text-align: left">
<ul>
  <li>
    <a href="people">Las personas:</a>
    quiénes somos, qué hacemos
  </li>
  <li>
    <a href="philosophy">Nuestra filosofía:</a>
    por qué y cómo lo hacemos
  </li>
  <li>
    <a href="../devel/join/">Implíquese:</a>
    cómo llegar a ser parte de Debian
  </li>
  <li>
    <a href="help">Contribuir:</a>
    Cómo puede ayudar a Debian
  </li>
  <li>
    <a href="../social_contract">Contrato social de Debian:</a>
    nuestro programa ético
  </li>
  <li>
    <a href="diversity">Todas las personas son bienvenidas:</a>
          Declaración de diversidad de Debian
  </li>
  <li>
    <a href="../code_of_conduct">Para participantes:</a>
          Código de conducta de Debian
  </li>
  <li>
    <a href="../partners/">Socios:</a>
    empresas y organizaciones que proporcionan asistencia continuada al proyecto Debian
  </li>
  <li>
    <a href="../donations">Donaciones:</a>
          Cómo patrocinar el proyecto Debian
  </li>
  <li>
    <a href="../legal/">Información legal:</a>
          licencias, marcas registradas, política de privacidad, política de patentes, etc.
  </li>
  <li>
    <a href="../contact">Contacto:</a>
          Cómo ponerse en contacto con nosotros
  </li>
</ul>
    </div>

  </div>

 <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
<h2>Debian es un sistema operativo</h2>
<p>Debian es un sistema operativo libre, desarrollado y mantenido por el proyecto Debian. Una distribución libre de Linux con miles de aplicaciones para cumplir con las necesidades de nuestros usuarios.</p>
    </div>

    <div style="text-align: left">
<ul>
  <li>
    <a href="../distrib">Descargas:</a>
    dónde conseguir Debian
  </li>
  <li>
  <a href="why_debian">Por qué Debian:</a>
          razones para elegir Debian
  </li>
  <li>
    <a href="../support">Soporte:</a>
          dónde obtener ayuda
  </li>
  <li>
    <a href="../security">Seguridad:</a>
    última actualización <br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages">Software:</a>
    buscar y navegar por la larga lista de paquetes Debian
  </li>
  <li>
    <a href="../doc">Documentación:</a>
          guía de instalación, preguntas frecuentes, «CÓMOs», wiki, y más
  </li>
  <li>
    <a href="../bugs">Sistema de seguimiento de fallos («BTS»):</a>
          cómo informar de un fallo, documentación del «BTS»
  </li>
  <li>
    <a href="https://lists.debian.org/">Listas de correo: </a>
          colección de listas de Debian para usuarios, desarrolladores, etc.
  </li>
  <li>
    <a href="../blends">Mezclas («blends»):</a>
    metapaquetes para necesidades específicas
  </li>
  <li>
    <a href="../devel">El rincón del desarrollador:</a>
    información de interés para desarrolladores de Debian
  </li>
  <li>
    <a href="../ports"> Adaptaciones/Arquitecturas:</a>
    soporte de Debian para distintas arquitecturas de CPU
  </li>
  <li>
    <a href="search">Búsqueda:</a>
          información sobre cómo usar el motor de búsquedas de Debian
  </li>
  <li>
    <a href="cn">Idiomas:</a>
          ajustes de idioma para el sitio web de Debian
  </li>
</ul>
    </div>
  </div>

</div>

