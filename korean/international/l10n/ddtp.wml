#use wml::debian::template title="데비안 설명 번역 프로젝트 &mdash; DDTP"
#use wml::debian::toc
#use wml::debian::translation-check translation="7b06966ef55556a97589982c5f8d5e1c022b075e" maintainer="Sebul"

<p><a href="https://ddtp.debian.org">Debian Description Translation Project</a>
(<a href="mailto:Michael%20Bramer%20%3Cgrisu@debian.org%3E">Michael Bramer</a>가 구현)는 
번역된 패키지 설명과 번역 지원을 위한 인프라를 제공하는 것이 목표입니다. 
이미 몇 년 동안 존재했지만 데비안 호스트 break-in 이후 비활성화되었으며 현재는 과거에 비해 기본적인 기능만 있습니다.
</p>

<p>
이 프로젝트가 지원하는 것:
</p>
<ul>
  <li>현재 (sid) 및 이전 패키지 설명 가져오기.</li>
  <li>다른 패키지 설명의 한 설명에서 이미 번역된 단락 재사용.</li>
  <li>미러 및 APT용 <tt>Translation-*</tt> 파일 제공.</li>
</ul>

<p>
과거부터 알려진 리뷰 프로세스가 아직 활성화되지 않았습니다. 
또한, 데비안 보관소의 비자유 섹션은 번역할 수 없는데 왜냐면 주의 깊게 확인해야 하는 라이선스 문제가 있을 수 있기 때문입니다.
<!-- I thought that even for non-free projects the debian packaging stuff
     (debian/) is free but it seems it''s not required!? -->

</p>

<p>
56000개 넘는 패키지 기록을 번역하는 것은 큰 도전입니다. 
우리의 목표를 달성할 수 있도록 도와주세요. 
남은 이슈는 <a href="#todo">할 일</a> 목록도 보십시오.
</p>

<toc-display/>

<toc-add-entry>DDTP 인터페이스</toc-add-entry>

<p>
모든 인터페이스는 DDTP 백엔드를 사용하므로 해당 언어가 이미 지원되는지 먼저 확인해야 합니다. 
대부분 언어에 해당될 것입니다. 
이러한 지원이 존재하지 않으면 <email debian-i18n@lists.debian.org>에 써서 해당 언어를 활성화할 수 있습니다.

<h3 id="DDTSS">웹 프론트엔드</h3>
<p>
<a href="mailto:Martijn%20van%20Oosterhout%20%3Ckleptog@gmail.com%3E">Martijn van
Oosterhout</a>가 만든 멋진 웹 프론트엔드 
<a href="https://ddtp.debian.org/ddtss/index.cgi/xx">DDTSS</a>가 있습니다.
</p>

<h4>개요</h4>
<p>
번역 및 교정을 추가로 허용합니다. 
각 언어 팀에 대한 사용자 정의 구성을 지원하므로, 
각 팀은 설명이 DDTP로 전송될 때까지 몇 개의 리뷰가 필요한지 결정할 수 있습니다. 
제한된 그룹의 사용자만 특정 작업을 수행할 수 있도록 사용자 권한을 요청할 수도 있습니다. 
또한 인코딩에 신경 쓸 필요가 없습니다. DDTSS가 이 작업을 처리합니다.
</p>

<p>
현재 기본 속성:
</p>
<dl>
  <dt>number of reviews:</dt><dd>3</dd>
  <dt>supported languages:</dt><dd>DDTP 모두</dd>
  <dt>user authorisation:</dt><dd>아뇨, 모두에게 열려있음</dd>
</dl>

<p>
언어의 기본 단어 목록을 지정할 수 있습니다. 
도구 설명을 통해 기본 번역을 제공하는 데 쓰입니다. 
이 목록은 언어 페이지 아래에 있는 링크를 통해 사용가능합니다.
</p>

<h4>워크플로우</h4>
<p>
DDTSS는 아래 아이템을 모든 언어에 대해 제공:
</p>

<h5>Pending translation</h5>
<p>
보류 중인 번역 목록입니다. 
사용자가 자유롭게 번역할 수 있는 설명입니다. 다음과 같습니다:
</p>
<pre>
exim4 (priority 52)
exim4-config (priority 52)
ibrazilian (priority 47, busy)
postgresql-client (priority 47)
postgresql-contrib (priority 47)
grap (priority 45)
</pre>

<p>
언어 팀은 높은 우선순위를 갖는 패키지(이는 필수, 기본 등 카테고리를 써서 계산됨)를 번역해야 합니다. 
이를 위해 패키지가 정렬됩니다.
</p>

<p>
busy로 표시된 설명은 이미 사용자가 예약했으며 최대 15분 동안 선택할 수 없습니다. 
이 시간 동안 커밋이 없으면 자유롭게 사용 가능한 것으로 다시 표시됩니다.
</p>

<p>
설명은 프론트엔드가 수락하기 전에 완전히 번역해야 합니다. 
따라서 시작하기 전에 텍스트 전체를 번역할 수 있는지 확인하십시오. 
번역을 추가하려면 <q>Submit</q>을 선택하고 번역을 안 하려면 <q>Abandon</q>을 선택하세요. 
또한 운이 좋으실 수도 있고 이미 이전 버전의 영어 템플릿에 대한 번역본이 있을 수도 있으며, 
번역본에 통합해야 하는 영어 번역본의 변경사항도 다를 수 있습니다. 
페이지 아래에서 옛 번역을 복사하여 붙여넣고 적절히 업데이트할 수 있습니다.
</p>

<p>
# Does not yet work as expected
텍스트 너비가 추하게 바뀌는 것을 막으려면 필요한 경우(예: 목록 항목)가
아니면 개행을 수동으로 입력하지 마세요. 줄이 자동으로 끊어집니다. 사용자는
교정 중에 사소한 부분을 더하거나 뺄 수 있으므로, 수동으로 개행을
입력하면 줄 길이가 일관되지 않을 수 있습니다. 이것을 수정하면 리뷰에서 생성된
diff를 읽기 어려워집니다.
</p>

<p>
선호 패키지를 이름으로 선택할 수도 있습니다. 
이 기능은 manpages-de, manpages-es 처럼 비슷한 패키지 집합을 번역하고 이전 번역을 복사하여 붙여넣을 때 유용합니다.
</p>

<p>
이미 번역된 패키지도 이 방법으로 다시 가져올 수 있습니다.
(현재 DDTP 기능은 버그가 있으므로 가능하면 이 기능을 사용하지 마시오)
</p>

<h5>Pending review</h5>
<p>
아직 리뷰가 필요한 번역된 설명 목록입니다. 이 목록은 다음과 같이 보일 겁니다:
</p>

<pre>
 1. aspell-es (needs review, had 1)
 2. bookmarks (needs initial review)
 3. doc-linux-ja-html (needs initial review)
 4. doc-linux-ja-text (needs initial review)
 5. gnome-menus (needs initial review)
 6. geany (needs review, had 2)
 7. initramfs-tools (needs initial review)
 8. inn2 (needs initial review)
</pre>

<p>
아래 플래그 있음:</p>
<dl>
    <dt lang="en">needs initial review:</dt>
    <dd>이 번역의 현 버전은 리뷰를 한번도 통과하지 않음.
    </dd>

    <dt lang="en">needs review:</dt>
    <dd>이 번역의 현 버전은 리뷰 필요, 그러나 적어도 한번 통과.
    </dd>

    <dt lang="en">reviewed:</dt>
    <dd>이 설명은 변경 없이 사용자가 리뷰.
    다른 사용자가 교정 필요.
    </dd>

    <dt lang="en">owner:</dt>
    <dd>이 설명은 사용자가 교정을 읽는 동안 번역되거나 변경되었습니다. 다른 사용자가 교정해야 합니다.
    </dd>
</dl>

<p>
수정이 있는 리뷰가 이미 수행된 경우 패키지를 선택하면 마지막 검토의 모든 변경 사항을 보여주는 멋진 색상 diff가 나타납니다.
</p>

<h5>Recently translated</h5>
<p>
DDTP로 전송된 설명 목록입니다.
최대 20개 패키지가 전송 날짜와 함께 나타납니다.
</p>

<h3 id="Pootle">국제화 프론트엔드</h3>
<p>
PO 파일, Debconf 템플릿 등 데비안의 다양한 문서 번역에 도움이 되는 새로운 프레임워크를 구현할 계획이 있었습니다. 
이 프레임워크는 언젠가 패키지 설명도 지원할 것입니다. 이 기능이 활성화되어 예상대로 동작하면 현재 DDTP와 그 프론트엔드가 끝날 겁니다.
</p>

<p>이 프레임워크는
<a href="http://pootle.locamotion.org/">Pootle</a> 기반이 될 겁니다. 그건 Google Summer of
Code <a href="https://wiki.debian.org/SummerOfCode2006?#Translation_Coordination_System">project</a>였습니다.
</p>

<toc-add-entry name="rules">공통 번역 규칙</toc-add-entry>
<p>
번역하는 동안 영어 설명을 변경하지 않는 것이 중요합니다. 
오류가 있으면 해당 패키지에 대해 버그 보고서를 제출해야 합니다. 
자세한 내용은 <a href="$(HOME)/Bugs/Reporting">데비안에서 버그를 보고하는 방법</a>을 참조하십시오.
</p>

<p>
각 첨부 파일의 번역 안 된 부분을 번역합니다. 
이 부분은 &lt;trans&gt;로 표시되어 있습니다. 
첫 번째 열에 마침표만 포함된 행은 변경하지 않는 것이 중요합니다. 
이들은 문단 구분자이며 APT 프론트엔드에서 안 보입니다.
</p>

<p>
이미 번역된 문단은 다른 설명 또는 이전 번역에서 재사용됩니다
(따라서 이 최초 영문 문단은 그 이후 변경되지 않았음을 나타냄). 
이러한 문단을 변경하면 동일한 문단의 다른 모든 설명도 변경되지 않습니다.
</p>

<p>
또한 각 언어 팀에는 단어 목록이나 인용 스타일과 같은 자체 환경설정이 있습니다. 
가능한 한 이 기준을 준수해 주십시오. 
가장 중요한 규칙이 발표됩니다. 
먼저 <a href="#DDTSS">DDTSS</a>를 통해 또는 <a href="https://packages.debian.org/aptitude">aptitude</a> 같은 패키지 관리 시스템에서 설명을 검색하여 번역 선호도를 파악하여 기존 번역을 검토해 보는 것이 좋습니다. 
잘 모르면 언어 팀에 문의하십시오.
</p>

<toc-add-entry>교정 및 오류 수정</toc-add-entry>
# General proofread suggestions, not DDTSS specific
<p>
현재 DDTSS만 검토 모드를 구현하고 있으며 
이러한 번역본은 고정된 수의 교정을 통과한 DDTP에만 보냅니다.
</p>

<p>
일반적인 오타 또는 인코딩 문제와 같은 기타 오류를 쉽게 수정할 수 있는 경우, 
검토 프로세스를 생략하고 스크립트를 사용하여 모든 패키지에 대해 한 번에 수정할 수 있습니다. 
신뢰할 수 있는 번역 코디네이터 한 명만 이러한 모든 문제를 수집하고 스크립트를 적용하는 것이 좋습니다.
</p>

<p>
교정에는 오랜 시간이 걸릴 수 있으므로(특히 사소한 문제만 항상 수정하는 경우), 
리뷰 중에 단순한 오타와 불일치를 무시하고 나중에 이러한 모든 문제(수집되기 바라는)에 대한 검사를 시작하는 것이 옵션일 수 있습니다. 
이렇게 하면 리뷰 속도가 빨라지고 이러한 수정 사항을 모든 설명의 뒷부분에 적용할 수 있습니다.
</p>

<toc-add-entry>번역 사용법</toc-add-entry>
<p>
APT 패키지가 <a href="https://packages.debian.org/lenny/admin/apt">lenny</a>부터 가능하므로 
번역된 패키지 설명을 적절하게 지원할 수 있습니다. 
이 패키지를 사용하면 모든 사용자가 APT를 사용하는 모든 프로그램에서 원하는 언어로 설명을 읽을 수 있습니다. 
여기에는 <tt>apt-cache</tt>, <tt>aptitude</tt>, <tt>synaptic</tt> 및 기타 다양한 기능이 들어갑니다.
</p>

<p>
APT는 Debian 미러에서 <tt>Translation-<var>lang</var></tt> 파일을 다운로드합니다. 
이 옵션은 lenny 및 최신 배포에만 사용할 수 있습니다. 
미러에서 이러한 파일의 위치는 
<a href="http://ftp.debian.org/debian/dists/sid/main/i18n/">dists/main/sid/i18n/</a>입니다.
</p>

<p>번역 사용을 불가능하게 할 수도 있습니다. 그러려면
</p>
<pre>
APT::Acquire::Translation "none";
</pre>
<p>을
<tt>/etc/apt/apt.conf</tt>에 추가하세요. <tt>none</tt> 대신 언어 코드도 제공됩니다.
</p>

<!--
<p>
FIXME: I need to test this script from me again against the new Translation-<lang>
files. Ignore this for now:<br />
There is also a small script available which just replaces English descriptions
in the local <tt>Packages</tt> files (<tt>/var/lib/apt/lists/</tt>) with
translations. This could be used if the new APT version cannot be installed for
whatever reasons.
</p>
-->

<toc-add-entry name="todo">할 일</toc-add-entry>

<p>
DDTP 진전이 좀 있지만 아직 할 일이 많습니다.
</p>
<ul>
  <li>
  모든 번역 팀은 정말 큰 패키지 목록을 처리하는 데 도움이 되는 새로운 번역자와 검토자를 찾습니다.
  </li>
  <li>
  번역/리뷰 프로세스 중에 영어 패키지 설명을 개선하기 위한 지원을 추가합니다. 
  개선된 설명이 포함된 새로운 영어 의사 언어를 성공적으로 리뷰 후 버그를 자동으로 파일로 추가하여 보관할 수 있습니다.
  </li>
  <li>
  Pootle을 기반으로 하는 새로운 국제화 프레임워크는 우리 아이디어를 모두 충족시킬 때까지 많은 노력이 필요합니다.
  </li>
</ul>
