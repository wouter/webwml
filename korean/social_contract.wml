#use wml::debian::template title="데비안 우리의 약속" BARETITLE=true
#use wml::debian::translation-check translation="c505e01dd6ca2b53d9a229a691d0c2b20c48b36b" maintainer="Changwoo Ryu"

{#meta#:
<meta name="keywords" content="social contract, dfsg, social contract,
dfsg, dfsg, debian social contract, dfsg">
:#meta#}

#  Original document: contract.html
#  Author           : Manoj Srivastava ( srivasta@tiamat.datasync.com )
#  Created On       : Wed Jul  2 12:47:56 1997

<p>1.1 버전을 2004년 4월 26일에 비준했습니다.
  <a href="social_contract.1.0">1.0 버전</a>(1997.7.5. 비준)을 대체합니다.
</p>

<p>데비안(데비안 시스템을 만든 사람들)은
<strong>데비안 우리의 약속</strong>을 만들었습니다.

그 중에서 <a href="#guidelines">데비안 자유 소프트웨어 지침(DFSG, Debian Free
Software Guidelines)</a> 부분은, 처음에는 우리가 준수하기로 동의한 약속의
집합으로 설계되었지만, 자유 소프트웨어 공동체에서 <a
href="https://opensource.org/docs/osd">오픈 소스 정의(Open Source
Definition)</a>의 기초로 채택했습니다.

<hr />
    <h2>자유 소프트웨어 공동체와의 <q>우리의 약속</q></h2>

    <ol>
      <li>
        <strong>데비안은 100% 자유로 남을 것입니다</strong>
        <p>
          우리는 <q><cite>데비안 자유 소프트웨어 지침 (The Debian Free
          Software Guidelines)</cite></q>이라 이름 지어진 문서에서
          저작물이 <q><em>자유</em></q>인지 결정하는 데 쓰는 지침을
          제공합니다. 우리는 데비안 시스템과 모든 구성요소가 이 지침에 따라
          자유로울 것임을 약속합니다. 우리는 데비안에서 자유와 비자유 작업
          모두 만들거나 사용하는 사람들을 지원할 것입니다. 우리는 시스템이
          비자유 소프트웨어의 사용을 요구하도록 만들지 않을 것입니다.
        </p>
      </li>
      <li><strong>우리는 자유 소프트웨어 공동체에 되돌려 줄 것입니다</strong>
        <p>
          데비안 시스템의 새로운 구성 요소를 작성할 때, 우리는 데비안 자유
          소프트웨어 지침과 일치하는 방식으로 라이선스를 부여할 것입니다.
          우리는 우리가 만들 수 있는 한 가장 좋은 시스템을 만들 것이며, 그렇게
          해서 자유 소프트웨어는 널리 배포되고 사용될 것입니다. 우리는 버그
          수정, 개선 건의, 사용자 제안 사항 등을 우리 시스템이 포함하는
          소프트웨어의 "<em>상위</em>" 개발자에게 전달할 것입니다.
        </p>
      </li>
      <li><strong>우리는 문제를 숨기지 않을 것입니다</strong>
        <p>
          우리는 모든 버그 보고 데이터베이스를 일반 대중에게 항상 공개되도록
          유지할 것입니다. 사용자가 온라인으로 제출한 보고는 즉시 다른 사람이
          볼 수 있게 됩니다.
        </p>
      </li>
      <li><strong>우리에게는 우리의 사용자와 자유 소프트웨어가 가장 우선합니다</strong>
        <p>
          우리는 우리의 사용자와 자유 소프트웨어 공동체의 요구에 따를
          것입니다. 우리는 그들의 이익을 1순위로 할 것입니다. 우리는 우리
          사용자들이 다양한 컴퓨터 운영환경에서 데비안을 운용하는 데 필요한
          사항들을 제공할 것입니다. 우리는 데비안 시스템에서 동작하려는
          자유롭지 않은 소프트웨어에 반대하지 않고, 그러한 소프트웨어를
          만들거나 사용하는 사람들에게 비용을 요구하지도 않을 것입니다. 다른
          이들이 데비안 시스템과 다른 소프트웨어 모두 들어 있는 배포판을
          만드는 것도 어떤 대가 지불 필요도 없이 허락할 것입니다. 이러한
          목표를 촉진하기 위해 우리는 그러한 시스템 사용을 막는 어떤 법적인
          제한이 없는 고품질의 통합 시스템을 제공할 것입니다.
        </p>
      </li>
      <li><strong>우리의 자유 소프트웨어 규격에 맞지 않는 프로그램</strong>
        <p>
          우리는 일부 사용자가 <a href="#guidelines">데비안 자유 소프트웨어
          지침</a>에 따르지 않는 프로그램을 사용할 필요가 있다는 것을 알고
          있습니다. 우리의 아카이브에 이런 소프트웨어에 대한 보관 장소를
          <q><code>contrib</code></q>와 <q><tt>non-free</tt></q> 영역에 만들었습니다.
          이 디렉터리의 소프트웨어는 데비안 시스템의 일부는 아니지만
          데비안에서 원활히 사용할 수 있도록 조정되어 있습니다. 우리는
          CD 제조업자들이 이 디렉터리의 소프트웨어 패키지들 각각의
          라이선스를 검토한 후에 그들의 CD로 해당 소프트웨어를
          배포할 수 있는지 결정하도록 권고합니다. 이런 방식으로 우리는
          데비안 시스템의 일부가 아닌 자유롭지 않은 소프트웨어의 사용을
          지원합니다. 그리고 이들 소프트웨어 패키지들에게 기본적인 것(버그
          추적 시스템과 메일링 리스트 같은 것)을 제공합니다.
        </p>
      </li>
    </ol>
<hr />
<h2 id="guidelines">데비안 자유 소프트웨어 지침(DFSG, The Debian Free Software Guidelines)</h2>
<ol>
   <li><p><strong>자유로운 재배포(Free Redistribution)</strong></p>
     <p>데비안 구성요소의 라이선스는 소프트웨어의 판매나 무상 제공에 대해
     어떠한 부분에 대해서라도 제한하지 않아야 합니다. 이 때 소프트웨어는
     각각의 소스로부터 생성된 프로그램을 포함한 전체 소프트웨어 배포판의
     구성요소를 뜻합니다. 또한 그 라이선스는 저작권 사용료(royalty)나 판매에
     대한 비용 지불을 요구하면 안 됩니다.</p></li>
   <li><p><strong>소스 코드(Source Code)</strong></p>
     <p>프로그램은 반드시 소스 코드를 포함해야 하며, 컴파일한 프로그램 뿐만
     아니라 소스 코드에 대한 배포도 허용해야 합니다.
     </p></li>
   <li><p><strong>파생 저작물(Derived Works)</strong></p>
     <p>라이선스는 소프트웨어의 수정과 그 소프트웨어를 토대로 한 다른 저작물도
     허용해야 합니다. 그리고 그것에 대해 원래 소프트웨어의 라이선스와 같은
     조건으로 배포를 허용해야 합니다.
     </p></li>
   <li><p><strong>소스 코드를 완전한 상태로 유지(Integrity of The Author's Source Code)</strong></p>
     <p>프로그램 생성 시점에서 변형을 목적으로 소스 코드에 포함한
     <q><tt>패치 파일</tt></q>을 사용 허가가 배포를 허용한 경우에는,
     소스 코드를 <strong>오직</strong> 패치된 형태만으로 배포하도록
     라이선스는 제한할 수 있습니다. 라이선스는 변형한 소스 코드로부터
     생성된 소프트웨어 배포를 명시적으로 허용해야 합니다. 라이선스는
     파생된 저작물이 다른 명칭이나 버전 번호를 갖도록 요구할 수
     있습니다(<em>이것은 절충안입니다. 데비안 공동체는 모든 저작자가
     어떤 파일, 소스, 바이너리의 수정 작업에도 제한을 두지 않도록
     권고합니다</em>).</p></li>
   <li><p><strong>개인 또는 단체에 대한 차별 금지(No Discrimination Against Persons or Groups)</strong></p>
     <p>라이선스는 어떠한 개인이나 단체에 대해서도 차별이 있어선 안
     됩니다.</p></li>
   <li><p><strong>사용 분야에 대한 차별 금지(No Discrimination Against Fields of Endeavor)</strong></p>
     <p>라이선스는 어떤 누구에게도 프로그램을 특정 분야에 사용하는 것을
     제한해서는 안 됩니다. 예를 들면 프로그램을 상업이나 유전 연구에 사용하는
     것을 제한해서는 안 될 것입니다.</p></li>
   <li><p><strong>라이선스 배포(Distribution of License)</strong></p>
     <p>프로그램에 첨부한 권리는 프로그램을 재배포 받은 모든 사람에게 권리
     부여자에 의한 추가적인 사용 허가 절차 없이 적용되어야 합니다.
     </p></li>
   <li><p><strong>라이선스는 데비안에 한정하지 않아야 한다(License Must Not Be Specific to Debian)</strong></p>
     <p>프로그램이 데비안 시스템의 일부일 경우에만 그 프로그램에 첨부된 권리가
     적용되도록 해서는 안 됩니다. 프로그램을 데비안에서 분리하여, 프로그램의
     사용 허가 조건을 다르게 하여 데비안 밖에서 배포하고 사용하더라도,
     프로그램을 재배포 받은 모든 사람은 데비안 시스템으로 결합되어 있을 때와
     같은 권리를 갖도록 해야 합니다.</p></li>
   <li><p><strong>사용 허가가 다른 소프트웨어에 악영향을 주어선 안 된다(License Must Not Contaminate Other Software)</strong></p>
     <p>사용 허가가 이 사용 허가된 소프트웨어와 더불어 배포되는 다른
     소프트웨어에 제한을 두면 안 됩니다. 예를 들면 라이선스는 같은 매체에
     배포되는 다른 모든 프로그램이 자유 소프트웨어여야 한다고 요구해서는 안
     됩니다.</p></li>
   <li><p><strong>라이선스의 예(Example Licenses)</strong>
     <p><q><strong><a href="https://www.gnu.org/copyleft/gpl.html">GNU 일반 공중 사용 허가서(GPL)</a></strong></q>,
     <q><strong><a href="https://opensource.org/licenses/BSD-3-Clause">버클리 소프트웨어 배포 사용 허가서(BSD)</a></strong></q>, 그리고 
     <q><strong><a href="https://perldoc.perl.org/perlartistic.html">Artistic</a></strong></q> 라이선스가
     우리가 <q><em>자유롭다고</em></q> 생각하는 사용 허가의 예입니다.</p></li>
</ol>

<p><em>우리의 <q>자유 소프트웨어 공동체로서 사회 계약(Social Contract)</q>을
정하자는 구상은 Ean Schuessler가 제안했습니다. Bruce Perens가 이 문서의 초안을
썼고, 1997년 6월 한 달 동안 다른 데비안 개발자들이 이메일을 통한 협의를 통해
다듬었으며, 그 후에 데비안 프로젝트의 공개적으로 정해진 정책으로
<a href="https://lists.debian.org/debian-announce/debian-announce-1997/msg00017.html">\
받아들여졌습니다.</a></em></p>

<p><em>Bruce Perens는 나중에 데비안 자유 소프트웨어 지침(Debian FreeSoftware
Guideline)에서 데비안에만 해당하는 사항을 제거하고
<a href="https://opensource.org/docs/definition.php"><q>오픈소스 정의(Open Source Definition)</q></a>를 
만들었습니다.</em></p>

<p><em>다른 단체들이 이 문서를 토대로 새 문서를 작성해도 됩니다. 그렇게 할
경우 데비안 프로젝트를 출처로 밝히기 바랍니다.
</em></p>
