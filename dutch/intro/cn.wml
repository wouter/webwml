#use wml::debian::template title="De Debian webpagina's in verschillende talen" BARETITLE=true
#use wml::debian::translation-check translation="de75d9bf72c9046fb3bbae3fcf7df540c6e2b420"
#use wml::debian::toc

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<define-tag toc-title-formatting endtag="required">%body</define-tag>
<define-tag toc-item-formatting endtag="required">[%body]</define-tag>
<define-tag toc-display-begin><p></define-tag>
<define-tag toc-display-end></p></define-tag>

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#intro">Inhoudsgebonden navigeren</a></li>
    <li><a href="#howtoset">De taal instellen in een webbrowser</a></li>
    <li><a href="#override">De instellingen overschrijven</a></li>
    <li><a href="#fix">Probleemoplossing</a></li>
  </ul>
</div>

<h2><a id="intro">Inhoudsgebonden navigeren</a></h2>

<p>
Een team <a href="../devel/website/translating">vertalers</a>
werkt aan de website van Debian om deze om te zetten naar een groeiend aantal
verschillende talen. Maar hoe werkt het overschakelen naar een taal in
een webbrowser? Een standaard die men
<a href="$(HOME)/devel/website/content_negotiation">content negotiation</a>
(onderhandelen over inhoud) noemt, stelt gebruikers in staat de taal (talen)
van hun voorkeur in te stellen voor het bekijken van de inhoud van webpagina's.
Welke versie van de pagina men te zien krijgt wordt onderhandeld tussen de
webbrowser en de webserver: de browser stuurt de voorkeuren naar de server en
daarop beslist de server welke versie geleverd wordt (op basis van de
voorkeuren van de gebruiker en de beschikbare versies).
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="http://www.w3.org/International/questions/qa-lang-priorities">Meer lezen op W3C</a></button></p>

<p>
Niet iedereen is op de hoogte van het mechanisme van onderhandelen over inhoud, daarom verwijzen onderaan iedere webpagina van Debian links naar de andere beschikbare versies. Merk op dat een andere taal selecteren uit deze lijst, enkel invloed heeft op de huidige pagina. Dit wijzigt de standaardtaal van uw webbrowser niet. Indien u een link naar een andere pagina volgt, zult u deze terug in de standaardtaal te zien krijgen.
</p>

<p>
Om uw standaardtaal te wijzigen, heeft u twee opties:
</p>

<ul>
  <li><a href="#howtoset">Uw webbrowser configureren</a></li>
  <li><a href="#override">De taalvoorkeuren van uw browser overschrijven</a></li>
</ul>

<p>
Ga direct naar de configuratie-instructies voor deze webbrowsers: <toc-display />
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> De originele taal van de website van Debian is het Engels. Daarom is het een goed idee om Engels (<code>en</code>) toe te voegen onderaan uw lijst met talen. Dit werkt als een back-up voor het geval een pagina nog niet is vertaald in een van uw voorkeurstalen.</p>
</aside>

<h2><a id="howtoset">De taal instellen in een webbrowser</a></h2>

<p>
Enkele algemene opmerkingen voordat we beschrijven hoe u de taalinstellingen in
verschillende webbrowsers kunt configureren. Ten eerste is het een goed idee om
alle talen die u spreekt op te nemen in uw lijst van voorkeurstalen. Als u
bijvoorbeeld Nederlands als moedertaal heeft, kunt u <code>nl</code> als eerste
taal kiezen, gevolgd door Engels met de taalcode <code>en</code>.
</p>

<p>
Ten tweede kunt u in sommige browsers taalcodes invoeren in plaats van uit een menu te kiezen. Als dat het geval is, houd er dan rekening mee dat het maken van een lijst zoals <code>nl, en</code> uw voorkeur niet bepaalt. In plaats daarvan worden gelijk gerangschikte opties gedefinieerd, en de webserver kan besluiten de volgorde te negeren en gewoon een van de talen te kiezen. Als u een echte voorkeur wilt aangeven, moet u werken met zogenaamde kwaliteitswaarden, d.w.z. zwevendekommawaarden tussen 0 en 1. Een hogere waarde geeft een hogere prioriteit aan. Als we teruggaan naar het voorbeeld met de Nederlandse en de Engelse taal, kunt u het bovenstaande voorbeeld als volgt wijzigen:
</p>

<pre>
nl; q=1.0, en; q=0.5
</pre>

<h3>Wees voorzichtig met landcodes</h3>

<p>
Een webserver die een verzoek ontvangt voor een document met de voorkeurstaal
<code>en-GB, nl</code> levert <strong>niet altijd</strong> de Engelse versie
vóór de Nederlandse. Dit gebeurt alleen als er een pagina met de taalextensie
<code>en-gb</code> bestaat. Het werkt echter wel andersom: een server kan een
<code>en-us</code>-pagina leveren als alleen <code>en</code> is opgenomen in de
lijst met voorkeurstalen.
</p>

<p>
Wij raden daarom aan om geen tweeletterige landcodes toe te voegen zoals
<code>en-GB</code> of <code>en-US</code>, tenzij u daar een heel goede reden
voor heeft. Als u er toch een toevoegt, zorg er dan voor dat u ook de taalcode
zonder de extensie opneemt: <code>en-GB, en, nl</code>
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://httpd.apache.org/docs/current/content-negotiation.html">Meer over onderhandelen over inhoud</a></button></p>

<h3>Instructies voor verschillende webbrowsers</h3>

<p>
We hebben een lijst samengesteld met populaire webbrowsers en enkele
instructies voor het wijzigen van de voorkeurstaal voor webinhoud in hun
instellingen:
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="chromium">Chrome/Chromium</toc-add-entry></strong><br>
  Open rechtsboven het menu en klik op <em>Instellingen</em> -&gt; <em>Geavanceerd</em> -&gt; <em>Talen</em>. Open het menu van <em>Talen</em> en u krijgt een lijst met talen te zien. Klik op de drie stippen naast een item om de volgorde te wijzigen. Indien nodig kunt u ook nieuwe talen toevoegen.</li>
        <li><strong><toc-add-entry name="elinks">ELinks</toc-add-entry></strong><br>
  De standaardtaal instellen via <em>Instellingen</em> -&gt; <em>Taal</em> verandert ook de taal die bij websites wordt aangevraagd. U kunt dit gedrag aanpassen en de <em>Accept-Language header</em> verfijnd instellen bij <em>Instellingen</em> -&gt; <em>Optiebeheer</em> -&gt; <em>Protocollen</em> -&gt; <em>HTTP</em></li>
        <li><strong><toc-add-entry name="epiphany">Epiphany</toc-add-entry></strong><br>
  Open in het hoofdmenu <em>Voorkeuren</em> en ga naar het tabblad <em>Taal</em>. Hier kunt u talen toevoegen, verwijderen en ordenen.</li>
        <li><strong><toc-add-entry name="mozillafirefox">Firefox</toc-add-entry></strong><br>
  Open <em>Voorkeuren</em> in de menubalk bovenaan. Scrol naar beneden naar <em>Taal en uiterlijk</em> -&gt; <em>Taal</em> in het paneel <em>Algemeen</em>. Klik op de knop <em>Kiezen</em> om uw voorkeurstaal in te stellen voor het weergeven van websites. In hetzelfde dialoogvenster kunt u ook talen toevoegen, verwijderen en herordenen.</li>
        <li><strong><toc-add-entry name="ibrowse">IBrowse</toc-add-entry></strong><br>
  Ga naar <em>Voorkeuren</em> -&gt; <em>Instellingen</em> -&gt; <em>Netwerk</em>. <em>Taal aanvaarden</em> geeft waarschijnlijk een * weer wat de standaard is. Wanneer u klikt op de knop <em>Taaldefinitie</em> zou u in staat moeten zijn om uw voorkeurstaal toe te voegen. Zoniet, kunt u ze handmatig toevoegen.</li>
        <li><strong><toc-add-entry name="icab">iCab</toc-add-entry></strong><br>
  <em>Bewerken</em> -&gt; <em>Voorkeuren</em> -&gt; <em>Browser</em> -&gt; <em>Lettertypes, Talen</em></li>
        <li><strong><toc-add-entry name="iceweasel">IceCat (Iceweasel)</toc-add-entry></strong><br>
  <em>Bewerken</em> -&gt; <em>Voorkeuren</em> -&gt; <em>Inhoud</em> -&gt; <em>Talen</em> -&gt; <em>Kiezen</em></li>
        <li><strong><toc-add-entry name="ie">Internet Explorer</toc-add-entry></strong><br>
  Klik op het pictogram <em>Gereedschap</em>, selecteer <em>Internetopties</em>, ga naar het tabblad <em>Algemeen</em> en klik op de knop <em>Talen</em>. Klik op <em>Taalvoorkeuren instellen</em> en in het volgende dialoogvenster kunt u talen toevoegen, verwijderen en herordenen.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="konqueror">Konqueror</toc-add-entry></strong><br>
        Bewerk het bestand <em>~/.kde/share/config/kio_httprc</em> en voeg er de volgende nieuwe regel aan toe:<br>
        <code>Languages=nl;q=1.0, en;q=0.5</code></li>
        <li><strong><toc-add-entry name="lynx">Lynx</toc-add-entry></strong><br>
        Bewerk het bestand <em>~/.lynxrc</em> en voeg de volgende regel toe:<br>
        <code>preferred_language=nl; q=1.0, en; q=0.5</code><br>
        U kunt ook de instellingen van de browser openen door op [O] te drukken. Scrol omlaag naar <em>Voorkeurstaal</em> en voeg het bovenstaande toe.</li>
        <li><strong><toc-add-entry name="edge">Microsoft Edge</toc-add-entry></strong><br>
        <em>Instellingen en meer</em>  -&gt; <em>Instellingen</em> -&gt; <em>Talen</em> -&gt; <em>Talen toevoegen</em><br>
        Klik op de knop met drie stippen naast een taal voor meer opties en om de volgorde te wijzigen.</li>
        <li><strong><toc-add-entry name="opera">Opera</toc-add-entry></strong><br>
        <em>Instellingen</em> -&gt; <em>Browser</em> -&gt; <em>Talen</em> -&gt; <em>Voorkeurstalen</em></li>
        <li><strong><toc-add-entry name="safari">Safari</toc-add-entry></strong><br>
        Safari gebruikt de instellingen voor het hele systeem op macOS en iOS. Dus om uw voorkeurstaal in te stellen, opent u <em>Systeemvoorkeuren</em> (macOS) of <em>Instellingen</em> (iOS).</li>
        <li><strong><toc-add-entry name="w3m">W3M</toc-add-entry></strong><br>
        Druk op [O] om het <em>Paneel optie-instellingen</em> te openen, scrol omlaag naar <em>Netwerkinstellingen</em> -&gt; <em>Accept-Language header</em>. Druk op [Enter] om de instellingen aan te passen (bijvoorbeeld <code>nl; q=1.0, en; q=0.5</code>) en bevestig met [Enter]. Scrol helemaal naar beneden naar [OK] om uw instellingen op te slaan.</li>
        <li><strong><toc-add-entry name="vivaldi">Vivaldi</toc-add-entry></strong><br>
        Ga naar <em>Instellingen</em> -&gt; <em>Algemeen</em> -&gt; <em>Taal</em> -&gt; <em>Aanvaarde talen</em>, klik op <em>Taal toevoegen</em> en kies er een uit het menu. Gebruik de pijltjes om de volgorde van uw voorkeuren aan te passen.</li>
      </ul>
    </div>
  </div>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Hoewel het altijd beter is om uw voorkeurstaal te selecteren in de browserconfiguratie, is er een optie om de instellingen te overschrijven met een cookie.</p>
</aside>

<h2><a id="override">De instellingen overschrijven</a></h2>

<p>
Als u om wat voor reden dan ook uw voorkeurstaal niet kunt instellen in de
instellingen van de browser, van het apparaat of van de computeromgeving, kunt
u als laatste redmiddel de voorkeuren overschrijven met behulp van een cookie.
Klik op een van de knoppen hieronder om een bepaalde taal bovenaan de lijst te
zetten.
</p>

<p>
Houd er rekening mee dat hierdoor een <a
href="https://en.wikipedia.org/wiki/HTTP_cookie">cookie</a> wordt ingesteld.
Uw browser zal die automatisch verwijderen wanneer u deze website een maand
niet bezoekt. U kunt de cookie natuurlijk altijd handmatig verwijderen in uw
webbrowser of door op de knop <em>Browserstandaard</em> te klikken.
</p>

<protect pass=2>
<: print language_selector_buttons(); :>
</protect>


<h2><a id="fix">Probleemoplossing</a></h2>

<p>
Soms verschijnt de website van Debian in de verkeerde taal ondanks alle
pogingen om een voorkeurstaal in te stellen. Onze eerste suggestie is om de
lokale cache (zowel op schijf als in het geheugen) in uw browser op te schonen
voordat u probeert om de website opnieuw te laden. Als u er absoluut zeker van
bent dat u <a href="#howtoset">uw browser juist hebt geconfigureerd</a>, dan
kan een defecte of verkeerd geconfigureerde cache het probleem zijn. Dit wordt
tegenwoordig een ernstig probleem, omdat steeds meer ISP's caching zien als een
manier om hun internetverkeer te verminderen.
Lees de <a href="#cache">paragraaf</a> over proxyservers, zelfs als u denkt dat
u er geen gebruikt.
</p>

<p>
Het is in elk geval altijd mogelijk dat er een probleem is met
<a href="https://www.debian.org/">www.debian.org</a>. Hoewel slechts een
handvol taalproblemen die de afgelopen jaren zijn gemeld, werden veroorzaakt
door een bug aan onze kant, is het heel goed mogelijk. Wij raden u daarom aan
eerst uw eigen instellingen en een mogelijke probleem met caching te
onderzoeken, voordat u <a href="../contact">contact</a> opneemt met ons. Als
<a href="https://www.debian.org/">https://www.debian.org/</a> wel werkt, maar
een van de <a href="https://www.debian.org/mirror/list">spiegelservers</a>
niet, gelieve dit te melden, zodat wij contact kunnen opnemen met de beheerders
van de spiegelserver.
</p>

<h3><a name="cache">Mogelijke problemen met proxyservers</a></h3>

<p>
Proxyservers zijn in wezen webservers die geen eigen inhoud hebben. Ze zitten
in het midden tussen gebruikers en echte webservers, pakken de verzoeken om
webpagina's op en halen de pagina's op. Daarna sturen ze de inhoud door naar de
webbrowser van de gebruikers, maar ze maken ook een lokale, in de cache
opgeslagen kopie die wordt gebruikt voor latere verzoeken. Dit kan het
netwerkverkeer aanzienlijk verminderen wanneer veel gebruikers dezelfde pagina
opvragen.
</p>

<p>
Hoewel dit meestal een goed idee is, veroorzaakt het ook storingen wanneer de
cache fouten bevat. Met name begrijpen sommige oudere proxyservers het
mechanisme van onderhandelen over inhoud niet. Dit heeft tot gevolg dat ze een
pagina in de ene taal in de cache opslaan en die vervolgens aanbieden, zelfs
als later om een andere taal wordt gevraagd. De enige oplossing is om de
caching-software op te waarderen of te vervangen.
</p>

<p>
In het verleden werden proxy-servers alleen gebruikt wanneer mensen hun
webbrowser dienovereenkomstig configureerden. Dit is echter niet langer het
geval. Het kan zijn dat uw internetprovider alle HTTP-verzoeken via een
transparante proxy omleidt. Als de proxy het onderhandelen over de inhoud niet
goed afhandelt, kunnen gebruikers in de cache opgeslagen pagina's in de
verkeerde taal te zien krijgen. De enige manier om dit te verhelpen is een
klacht indienen bij uw internetprovider, zodat deze zijn software kan
opwaarderen of vervangen.
</p>

