#use wml::debian::template title="Open Publicatie-licentie" NOCOPYRIGHT="true"
#use wml::debian::translation-check translation="d54152841794e2e9a156f6f2abb5445fb98cb218"

    <p><strong>v1.0, 8 juni 1999</strong></p>


    <h2 id="section1">I. VEREISTEN VOOR ZOWEL ONGEWIJZIGDE ALS GEWIJZIGDE VERSIES</h2>

    <p>De Open Publicatie-werken mogen geheel of gedeeltelijk worden
      gereproduceerd en gedistribueerd op elk fysiek of elektronisch medium op
      voorwaarde dat de voorwaarden van deze licentie worden nageleefd en dat
      deze licentie of een opname ervan door middel van verwijzing (met alle
      opties die door de auteur(s) en/of uitgever worden gekozen) wordt
      weergegeven in de reproductie.</p>

    <p>De juiste vorm voor een opneming door middel van verwijzing is als
      volgt:
      Copyright (c) &lt;jaar&gt; op &lt;naam van auteur of aangewezen
      persoon&gt;. Dit materiaal mag alleen worden verspreid onder de
      voorwaarden en bepalingen van de Open Publicatie-licentie, vX.Y of later
      (de laatste versie is momenteel beschikbaar op
      <url "http://www.opencontent.org/openpub/" />). De referentie moet
      onmiddellijk worden gevolgd door eventuele opties gekozen door de
      auteur(s) en/of uitgever van het document (zie <a href="#section6">sectie
      VI</a>).</p>

    <p>Commerciële herverspreiding van materiaal met Open Publicatie-licentie is
      toegestaan.</p>

    <p>Bij elke publicatie in de vorm van een standaardboek (op papier) moeten
      de naam van de oorspronkelijke uitgever en van de auteur worden vermeld.
      De naam van de uitgever en van de auteur worden op alle buitenkanten van
      het boek vermeld. Op alle buitenkanten van het boek moet de naam van de
      oorspronkelijke uitgever even groot zijn als de titel van het werk en
      moet deze met betrekking tot de titel als de bezitter ervan worden
      vermeld.</p>


    <h2 id="section2">II. COPYRIGHT</h2>

    <p>Het auteursrecht op elke Open Publicatie berust bij de auteur(s) of
      aangewezen persoon.</p>


    <h2 id="section3">III. TOEPASSINGSGEBIED VAN DE LICENTIE</h2>

    <p>De volgende licentievoorwaarden zijn van toepassing op alle
    Open Publicatie-werken, tenzij uitdrukkelijk anders vermeld in het
    document.</p>

    <p>Het louter samenvoegen van Open Publicatie-werken of een deel van een
    Open Publicatie-werk met andere werken of programma's op dezelfde media
    heeft niet tot gevolg dat deze licentie van toepassing is op die andere
    werken. Het samengestelde werk dient een kennisgeving te bevatten waarin de
    opname van het Open Publicatie-materiaal en een passende
    copyrightverklaring worden vermeld.</p>

    <p><em>SCHEIDBAARHEID</em>. Indien een deel van deze licentie in een
    rechtsgebied niet-afdwingbaar blijkt te zijn, blijven de overige delen van
    de licentie van kracht.</p>

    <p><em>GEEN GARANTIE</em>. Open Publicatie-werken worden in licentie
    gegeven en geleverd <q>zoals ze zijn</q> zonder enige vorm van garantie,
    expliciet of impliciet, met inbegrip van, maar niet beperkt tot, de
    impliciete garanties van verkoopbaarheid en geschiktheid voor een bepaald
    doel of een garantie van niet-inbreuk.</p>


    <h2 id="section4">IV. VEREISTEN VOOR GEWIJZIGDE WERKEN</h2>

    <p>Alle gewijzigde versies van documenten die onder deze licentie vallen,
    inclusief vertalingen, bloemlezingen, compilaties en deeldocumenten, moeten
    aan de volgende vereisten voldoen:</p>

    <ul>
      <li>De gewijzigde versie moet als zodanig worden gemarkeerd.</li>
      <li>De persoon die de wijzigingen aanbrengt moet worden geïdentificeerd
      en de wijzigingen moeten worden gedateerd.</li>
      <li>De vermelding van de oorspronkelijke auteur en de uitgever, indien
      van toepassing, moet worden gehandhaafd volgens de normale academische
      citeerpraktijken.</li>
      <li>De plaats waar het originele ongewijzigde document zich bevindt, moet
      worden bepaald.</li>
      <li>De naam/namen van de oorspronkelijke auteur(s) mag/mogen niet zonder
      de toestemming van de oorspronkelijke auteur(s) worden
      gebruikt om te beweren of te impliceren dat het resulterende document
      wordt goedgekeurd.</li>
    </ul>


    <h2 id="section5">V. AANBEVELINGEN VOOR GOEDE PRAKTIJKEN</h2>

    <p>Naast de vereisten van deze licentie, wordt het volgende gevraagd van en
    sterk aanbevolen aan herverdelers:</p>

    <ul>
      <li>Als u Open Publicatie-werken op papier of cd verspreidt, stelt u de
      auteurs ten minste dertig dagen voor het voltooien van het manuscript of
      de media per e-mail op de hoogte van uw voornemen tot herverspreiding, om
      de auteurs de tijd te geven bijgewerkte documenten aan te leveren. Deze
      kennisgeving moet eventuele wijzigingen aan het document beschrijven.</li>
      <li>Alle inhoudelijke wijzigingen (met inbegrip van schrappingen) moeten
      hetzij duidelijk in het document worden aangegeven, hetzij in een bijlage
      bij het document worden beschreven.</li>
      <li>Tenslotte, hoewel het niet verplicht is onder deze licentie, wordt
      het beschouwd als een goede gewoonte om aan de auteur(s) een gratis kopie
      aan te bieden van elke heruitgave op papier of cd van een werk met een
      Open Publicatie-licentie.</li>
    </ul>


    <h2 id="section6">VI. LICENTIEOPTIES</h2>

    <p>De auteur(s) en/of uitgever van een document met een
    Open Publicatie-licentie kunnen bepaalde opties kiezen door tekst toe te
    voegen aan de verwijzing naar of de kopie van de licentie. Deze opties
    worden beschouwd als onderdeel van het licentie-exemplaar en moeten samen
    met de licentie (of de opname ervan door middel van verwijzing) in
    afgeleide werken worden opgenomen.</p>

    <p>A. Om het verspreiden van inhoudelijk gewijzigde versies zonder de
    uitdrukkelijke toestemming van de auteur(s) te verbieden. <q>Inhoudelijke
    wijziging</q> wordt gedefinieerd als een wijziging van de semantische
    inhoud van het document, en sluit loutere wijzigingen van het formaat of
    typografische correcties uit.</p>

    <p>Om dit te bereiken, voegt u de zin <q>Distributie van substantieel
    gewijzigde versies van dit document is verboden zonder de uitdrukkelijke
    toestemming van de houder van het auteursrecht.</q> toe aan de
    licentieverwijzing of -kopie.</p>

    <p>B. Om te verbieden dat dit werk of afgeleide werken in hun geheel of
    gedeeltelijk worden gepubliceerd in de vorm van een standaardboek (op
    papier) voor commerciële doeleinden, tenzij daarvoor voorafgaande
    toestemming is verkregen van de houder van het auteursrecht.</p>

    <p>Om dit te bereiken voegt u de zin <q>Distributie van het werk of een
    afgeleide van het werk in enige standaardboekvorm (op papier) is verboden
    tenzij voorafgaande toestemming is verkregen van de houder van het
    auteursrecht.</q> toe aan de licentieverwijzing of -kopie.</p>
