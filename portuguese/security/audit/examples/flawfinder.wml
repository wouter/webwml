#use wml::debian::template title="Exemplo de auditoria automatizada: flawfinder"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="be191e77facf8c0d489cfd320232517e5233a3e2"

<p>O <a href="https://packages.debian.org/flawfinder">flawfinder</a> é um
escaneador de propósito geral para encontrar e reportar potenciais falhas tanto
em código-fonte C quanto C++.</p>


<h2>Executando flawfinder</h2>

<p>Executar o flawfinder é tão simples quanto invocar o comando com o nome de
diretórios ou arquivos para examinar. Se dado um nome de diretório, ele
processará todos os arquivos-fonte válidos que encontrar dentro daquele
diretório.</p>

<p>Além de fornecer ao programa uma lista de arquivos ou diretórios, existem
diversas opções de linha de comando que podem ser usadas para controlar o
comportamento da ferramenta.</p>

<p>Cada uma das opções é explicada nas páginas man, mas as seguintes opções
são particularmente úteis e serão usadas em nosso exemplo:</p>
<ul>
<li>--minlevel=X
<ul>
<li>Define um nível mínimo de risco X para inclusão na saída. Varia entre
1-5, com 1 sendo "baixo risco" e 5 sendo "alto risco".</li>
</ul></li>
<li>--html
<ul>
<li>Formata a saída como HTML em vez de texto simples.</li>
</ul></li>
<li>--context
<ul>
<li>Exibe o contexto, ou seja, a linha contendo a falha potencial.</li>
</ul></li>
</ul>

<p>Para produzir um arquivo HTML contendo os resultados de nosso programa,
somente se preocupando com funções de "alto risco", executaríamos algo
assim:</p>
<pre>
flawfinder --html --context --minlevel=4 test.c &gt; output.html
</pre>

<h2>Os resultados</h2>

<p>Executar o flawfinder contra nossa
<a href="test.c.html">amostra de código</a> produz a seguinte saída:</p>

<hr />
# Nota do tradutor: trecho não traduzido, pois trata-se da saída do programa
<div class="sampleblock">
<p>
Examining test.c <br>
<ul>
<li>test.c:18: <b>  [4] </b> (buffer) <i> strcpy:
  Does not check for buffer overflows when copying to destination.
  Consider using strncpy or strlcpy (warning, strncpy is easily misused).  </i>
<pre>
      strcpy( dir, argv[ 1 ] );
</pre>
<li>test.c:24: <b>  [4] </b> (buffer) <i> sprintf:
  Does not check for buffer overflows. Use snprintf or vsnprintf.  </i>

<pre>
	sprintf( dir, "%s", getenv( "HOME" ) );
</pre>
<li>test.c:33: <b>  [4] </b> (shell) <i> popen:
  This causes a new program to execute and is difficult to use safely.
  try using a library call that implements the same functionality if
  available.  </i>
<pre>
    fp = popen( cmd, "r" );
</pre>
<li>test.c:42: <b>  [4] </b> (format) <i> printf:
  If format strings can be influenced by an attacker, they can be
  exploited. Use a constant for the format specification.  </i>

<pre>
      printf( buff );
</pre>
</ul>
<p>
Number of hits = 4
<br>
Number of Lines Analyzed = 48 in 0.53 seconds (1392 lines/second)
<br>
</div>
<hr />

<h2>Entendendo a saída</h2>

<p>Muito parecida com a saída do <a href="RATS">RATS</a>, este relatório é
muito simples de ler. Ele claramente mostra as funções que foram detectadas
como potencialmente perigosas, como também uma descrição do problema.</p>

<p>A inclusão de informação contextual é muito útil também, já que ela pode
imediatamente atrair a atenção para áreas de interesse, ou rejeitar outros
relatórios como sendo inválidos.</p>

<p>A análise da <a href="test.c">nossa amostra de código</a> é claramente
inteligente, no sentido de que ela não avisou sobre <i>toda</i> utilização
da problemática função <tt>strcpy</tt> - somente aquelas que considerou serem
potencialmente perigosas.</p>

<p>Nesse sentido, ela conseguiu realçar todas as falhas do nosso código ao
mesmo tempo em que não teve falso positivos.</p>
<hr />
<p><a href="..">Voltar para o projeto de auditoria</a> | <a href="index">Voltar para a página de amostra de auditoria</a></p>
