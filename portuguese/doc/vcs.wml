#use wml::debian::ddp title="SCV do projeto de documentação do Debian"
#use wml::debian::toc
#use wml::debian::translation-check translation="bf1a0486dfbf35e11a7ff98a29d9e2e4f2eda3f3"

<p>
O projeto de documentação do Debian armazena suas páginas web e grande parte dos
textos dos manuais no serviço salsa do Debian em <strong>salsa.debian.org</strong>,
que é a instância GitLab do Debian.
Por favor leia a <a href="https://wiki.debian.org/Salsa">documentação do salsa</a>
para obter mais informações sobre como este serviço funciona.
</p>

<p>
Todas as pessoas podem baixar os fontes a partir do serviço salsa. Somente
membros(as) do projeto de documentação do Debian podem atualizar os arquivos.
</p>

<toc-display />

<toc-add-entry name="access">Acessando os fontes no git</toc-add-entry>

<p>
Você pode usar uma interface web para acessar os arquivos individualmente e ver
as alterações de cada projeto em
<url "https://salsa.debian.org/ddp-team/" />
</p>

<p>
Para baixar um manual inteiro, geralmente a melhor opção é o
acesso direto ao servidor git. Você precisa ter o pacote
<tt><a href="https://packages.debian.org/git">git</a></tt> na sua própria
máquina.
</p>

<h3>Clonando um repositório git anonimamente (modo apenas leitura)</h3>

<p>Use este comando para baixar todos os arquivos de um projeto:</p>

<p style="margin-left: 2em">
  <code>git clone https://salsa.debian.org/ddp-team/release-notes.git</code>
</p>

<p>
Faça o mesmo para todos os projetos que você deseja clonar localmente.
</p>

<h3>Clonando um repositório git com privilégios de envio (modo leitura e escrita)</h3>

<p>
Antes de acessar o servidor git usando esse método, você deve primeiro obter
acesso de escrita. Por favor, leia primeiro como
<a href="#obtaining">solicitar</a> permissão de envio (push).
</p>

<p>Use este comando para baixar todos os arquivos de um projeto:</p>

<p style="margin-left: 2em">
  <code>git clone git@salsa.debian.org:ddp-team/release-notes.git</code>
</p>

<p>
Faça o mesmo para todos os projetos que você deseja clonar localmente.
</p>

<h3>Buscando alterações do repositório git remoto</h3>

<p>
Para atualizar sua cópia local com quaisquer alterações feitas por outras
pessoas, entre no diretório <strong>manuals</strong> e execute o comando:
</p>

<p style="margin-left: 2em">
  <code>git pull</code>
</p>

<toc-add-entry name="obtaining">Obtendo privilégios de envio</toc-add-entry>

<p>
Os privilégios de envio (push) estão disponíveis para quem deseja participar
da redação dos manuais. Geralmente solicitamos que você tenha enviado alguns
patches úteis antes.
</p>

<p>
Depois de criar sua conta no <a href="https://salsa.debian.org/">salsa</a>,
por favor solicite os privilégios de envio clicando em <q>Request to join</q>
no grupo ou em qualquer um dos os projetos específicos em
<url "https://salsa.debian.org/ddp-team/" />.
Por favor após fazer isso envie um e-mail para debian-doc@lists.debian.org
mostrando o histórico gravado do seu trabalho no Debian.
</p>

<p>
Depois que sua solicitação for aprovada, você fará parte do grupo
<a href="https://salsa.debian.org/ddp-team/"><q>ddp-team</q></a> ou de um dos
seus projetos.
</p>

<hr />

<toc-add-entry name="updates">Mecanismo de atualização automática</toc-add-entry>

<p>
As páginas web publicadas do texto do manual são geradas em
www-master.debian.org como parte do processo regular de reconstrução do site,
que acontece a cada quatro horas.
</p>

<p>
O processo é configurado para baixar as versões mais recentes do pacote do
repositório, reconstruir cada manual e instalar os arquivos no subdiretório
<code>doc/manuals/</code> do site web.
</p>

<p>
Os arquivos de documentação gerados pelo script de atualização podem ser
encontrados em <a href="manuals/">https://www.debian.org/doc/manuals/</a>.
</p>

<p>
Os arquivos de log gerados pelo processo de atualização podem ser encontrados
em <url "https://www-master.debian.org/build-logs/webwml/" /> (o script é
denominado <code>7doc</code> e está sendo executado <code>frequentemente</code>
como parte do trabalho do cron).
</p>

# <p>Observe que esse processo gera novamente o diretório
# <code>/doc/manuals/</code>. O conteúdo do diretório <code>/doc/</code> é
# gerado a partir do <a href="/devel/website/desc">webwml</a> ou de outros
# scripts, como os que extraem certos manuais de seus pacotes.</p>
